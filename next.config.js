const withOffline = require("next-offline");

const config = {
  i18n: {
    locales: [
      'en',
      'ru',
      'ar',
    ],
    defaultLocale: 'en',
    localeDetection: true,
  },
  generateInDevMode: false,
  workboxOpts: {
    runtimeCaching: [
      {
        urlPattern: /^https?.*/,
        handler: 'NetworkFirst',
        options: {
          cacheName: 'https-calls',
          networkTimeoutSeconds: 15,
          expiration: {
            maxEntries: 150,
            maxAgeSeconds: 30 * 24 * 60 * 60, // 1 month
          },
          cacheableResponse: {
            statuses: [0, 200],
          },
        },
      },
    ],
  },
};

module.exports = withOffline(config);