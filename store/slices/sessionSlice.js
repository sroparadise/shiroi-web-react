import {
  createSlice,
} from '@reduxjs/toolkit';

const initialState = {
  token: false,
  isAuthenticated: false,
  username: null,
  lang: 'en',
};

const sessionSlice = createSlice({
  name: 'session',
  initialState,
  reducers: {
    setSession: (state, action) => {
      state.token = action.payload.token;
      state.username = action.payload.username;
      state.isAuthenticated = true;
    },
    unsetSession: (state, action) => {
      state.token = false;
      state.username = null;
      state.isAuthenticated = false;
    }
  }
});

export const { setSession, unsetSession } = sessionSlice.actions;

export default sessionSlice.reducer;