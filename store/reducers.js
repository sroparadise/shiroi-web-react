import { combineReducers } from 'redux';

import session from './slices/sessionSlice';

export default combineReducers({ 
  session,
});