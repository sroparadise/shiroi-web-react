# SHIROI.ONLINE front-end

Engines:
- [Next.js](https://nextjs.org/)
- [Redux Toolkit](https://redux-toolkit.js.org/)
- [Axios](https://github.com/axios/axios)

## How to use
Install it

```
npm install 
# or
yarn install
```

Run it
```
npm run dev
# or
yarn run dev
```


