import { Component } from 'react';
import { initializeStore } from '../store/store';

const isServer = typeof window === 'undefined';
const __STORAGE_IDENTIFIER__ = 'SHIROI_ONLINE';

function getOrCreateStore(initialState) {
  if (isServer) {
    return initializeStore(initialState);
  }

  if (!window[__STORAGE_IDENTIFIER__]) {
    window[__STORAGE_IDENTIFIER__] = initializeStore(initialState);
  }
  return window[__STORAGE_IDENTIFIER__];
}

const withReduxStore = App => {
  return class AppWithRedux extends Component {
    constructor(props) {
      super(props);
      this.reduxStore = getOrCreateStore(props.initialReduxState);
    }

    render() {
      return <App {...this.props} reduxStore={this.reduxStore} />;
    }
  };
};

export default withReduxStore;