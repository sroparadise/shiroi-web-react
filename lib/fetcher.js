import axios from 'axios';

const base = axios.create({
    baseURL: 'https://api.shiroi.online/v1'
});

export const get = (url, headers = {}) => base.get(url, {
    headers,
}).then(res => res.data);

export const post = (url, data, headers = {}) => base.post(url, data, {
    headers,
}).then(res => res.data);

export const instance = base;