import translations from '../translations';
import { useRouter } from 'next/router';

const useTranslation = () => {
    const router = useRouter();
    return translations[router.locale].render().type;
};

export default useTranslation;