import { useEffect } from 'react';
import Table from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import filterFactory, { selectFilter, Comparator } from 'react-bootstrap-table2-filter';
import useSWR from 'swr';
import { get } from '../../lib/fetcher';

const { SearchBar } = Search;
const QuestList = () => {
    const {
        data,
        error,
    } = useSWR('/info', get, {
        refreshInterval: false
    });

    const levels = [...Array(130)].map((_, idx) => ({
        value: idx + 1,
        label: idx + 1,
    }));

    const columns = [
        {
            dataField: 'Quest Name',
            text: 'Quest Name',
            headerStyle: () => ({ width: '420px' }),
        },
        {
            dataField: 'Required Level',
            text: 'Req. Lvl',
            headerStyle: () => ({ width: '120px' }),
            filter: selectFilter({
                options: levels,
                className: 'col-filter',
                withoutEmptyOption: false,
                placeholder: '∞',
                defaultValue: null,
                comparator: Comparator.EQ,
            }),
        },
        {
            dataField: 'EXP reward',
            text: 'EXP',
            sort: true,
            headerStyle: () => ({ width: '115px' }),
        },
        {
            dataField: 'SP Reward',
            text: 'SP',
            sort: true,
        },
        {
            dataField: 'SP EXP Reward',
            text: 'SP EXP',
            sort: true,
        },
        {
            dataField: 'Gold Reward',
            text: 'GOLD',
            sort: true,
        },
    ];

    useEffect(() => {

    })

    return (data && !error) ? (<>
        <ToolkitProvider
            keyField="#"
            data={data.map((i, index) => ({ ...i, index: `${index + 1}` }))}
            columns={columns}
            search
        >
            {
                props => (
                    <div>
                        <b>Search Quest:</b><SearchBar style={{ marginLeft: '10px' }} {...props.searchProps} placeholder="Name, Level.." />
                        <Table bordered={true} bootstrap4={true} {...props.baseProps} filter={filterFactory()} />
                    </div>
                )
            }
        </ToolkitProvider>

        {
            !data.length && (
                <p className="text-warning">No data</p>
            )
        }
    </>) : <i className="fa fa-spinner fa-pulse fa-fw"></i>;
};

export default QuestList;