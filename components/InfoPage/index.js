import { connect } from 'react-redux';
import { useState, useEffect } from 'react';
import { Container, Col, Card, Tabs, Tab, Row } from 'react-bootstrap';
import FadeIn from 'react-fade-in';

import RatesGeneral from './RatesGeneral';
import QuestList from './QuestList';

const InfoPage = ({
    isAuthenticated,
}) => {
    const [key, setKey] = useState('quests');

    let index = {
        'general': {
            title: 'Rates & General Information',
            component: <RatesGeneral />
        },
        'quests': {
            title: 'Quest Rewards',
            component: <QuestList />,
        },
    };

    return (
        <div className="page-wrapper space-top-100">
            <Container>
                <Row className="justify-content-center">
                    <Col xs={12}>
                        <FadeIn>
                            <Card bg="t-black" text="white">
                                <Card.Header>
                                    <Tabs
                                        className="bg-t-brown"
                                        variant="pills"
                                        activeKey={key}
                                        onSelect={(k) => setKey(k)}>
                                        {
                                            Object.keys(index).map(key => (
                                                <Tab key={key} eventKey={key} title={`${index[key].title}`} />
                                            ))
                                        }

                                    </Tabs>
                                </Card.Header>
                                <Card.Body className="ranking-wrapper pb-3">
                                    {index[key].component}
                                </Card.Body>
                            </Card>
                        </FadeIn>
                    </Col>
                </Row>
            </Container>

        </div>
    );
};

export default connect(
    (state) => ({
        isAuthenticated: state.session.isAuthenticated,
    }),
)(InfoPage);