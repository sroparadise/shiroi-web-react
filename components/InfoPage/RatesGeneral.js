import ServerCapacity from '../Navigation/ServerCapacity';
import ServerTime from '../Navigation/ServerTime';

const RatesGeneral = () => {
    return (
        <div className="pb-2 pl-4 pt-4">
            <div>
                <h3>Server Time (UTC): <ServerTime /></h3>
                <h3>Server Status: <ServerCapacity /></h3>
            </div>
            <h3>Global Configuration</h3>
            <ul>
                <li>Level Cap <b>130</b></li>
                <li>Gear Cap <b>15D</b></li>
                <li><b>10x</b> Job Quest experience</li>
                <li><b>30x</b> Quest gold reward</li>
                <li><b>20x</b> Quest SP reward</li>
                <li><b>15x</b> Exp ratio</li>
                <li><b>12x</b> Item drop ratio</li>
                <li><b>7x</b> Gold drop ratio</li>
                <li><b>5x</b> Alchemy increase</li>
                <li>Uniques respawn time <b>30min</b></li>
            </ul>
            <h3>Unique kill rewards</h3>
            <ul>
                <li><b>Demon Shaytan</b> [5 Silk until Level 110]</li>
                <li><b>Roc</b> [50 silk until level 130]</li>
                <li><b>Merich</b> [5 silk until level 130]</li>
                <li><b>Khulood</b> [5 silk until level 130]</li>
                <li><b>Karkadan</b> [5 silk until level 130]</li>
                <li><b>Kidemonas</b> [5 silk until level 130]</li>
                <li><b>Captain Ivy</b> [2 silk until level 64]</li>
                <li><b>Tiger Girl</b> [2 silk until level 64]</li>
                <li><b>Uruchi</b> [2 silk until level 76]</li>
                <li><b>Isyutaru</b> [3 silk until level 85]</li>
                <li><b>Medusa</b> [40 silk until level 130]</li>
                <li><b>Lord Yarkan</b> [4 silk until level 101]</li>
                <li><b>Cerberus</b> [3 silk until level 76]</li>
            </ul>
        </div>
    )
};

export default RatesGeneral;