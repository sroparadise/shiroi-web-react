import Head from 'next/head';
const Header = ({
    title,
}) => (
    <Head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0" />
        <meta name="theme-color" content="#000000" />
        <meta name="robots" content="index, follow" />
        <meta name="keywords" content="Shiroi SRO, Silkroad Private Server, D15, CAP130, silkroad online private server, best silkroad private server, silkroad online PVP Server, PVP, Silkroad, SRO, silkroad privado, isro, silkroad online, silkroad server, silkroad online server" />
        <meta property="og:url" content="https://shiroi.online/" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content={`${title} :: Shiroi Online`} />
        <meta property="og:description" content="- Level CAP 130
        - Max 15 Degree
        - 10x Job Quest experience
        - 30x Quest gold reward
        - 20x Quest SP reward
        - 15X Exp ratio
        - 12x Item drop ratio
        - 7x Gold drop ratio
        - 2x Alchemy increase
        - Zerk titles available at start
        - silk4vote and silk/uniques
        - Literally the best server" />
        <meta name="description" content="- Level CAP 130
        - Max 15 Degree
        - 10x Job Quest experience
        - 30x Quest gold reward
        - 20x Quest SP reward
        - 15X Exp ratio
        - 12x Item drop ratio
        - 7x Gold drop ratio
        - 2x Alchemy increase
        - Zerk titles available at start
        - silk4vote and silk/uniques
        - Literally the best server" />
        <meta property="og:image" content="https://shiroi.online/icons/icon-256.png" />
        <meta property="fb:app_id" content="3048169555458107" />
        <meta name="twitter:card" content="summary" />
        <title>{title} :: Shiroi Online</title>
        <link rel="manifest" href="/manifest.json"></link>
        <link rel="icon" href="/icon.png" />
        <link rel="apple-touch-icon" href="/icon.png"></link>
    </Head>
);
export default Header;