import {
    Form, 
    Button, 
    Container,
    Col,
    Row,
} from 'react-bootstrap';
import { toast } from 'react-toastify';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { instance as axios } from '../../lib/fetcher';

const BonusCode = ({ token }) => {
    const {
        register,
        handleSubmit,
        errors,
    } = useForm();

    const [loading, setLoading] = useState(false);

    const onSubmit = async ({
        code,
    }, e) => {
        try {
            const { data } = await axios.post('/code', {
                code,
            }, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });

            toast.success(<p><b>Great!</b> Enjoy your <b>{data.reward}</b> free silk!<br />Please give it a few seconds to appear on your account.</p>);
            e.target.reset();
        } catch (e) {
            const messages = {
                'validation_failed': `Code is not available!`,
                'invalid_code': `Code is not valid!`,
                'default': `Unknown error - contact administration!`,
                'bad_ipaddr': `VPN/Proxy detection returned bad status, please again later!`,
            };

            toast.error(messages[e.response.data.message || 'default']);
        } finally {
            setLoading(false);
        }
    };

    return (
        <Container>
        <Row className="justify-content-center">
            <Col md={6}>
                <Form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
                    <Form.Group>
                        <Form.Label>Claim a gift code:</Form.Label>
                        <Form.Control
                            name="code" type="text" ref={register({
                                required: true,
                                minLength: 6,
                                maxLength: 32,
                            })}
                            placeholder="Enter your code" />
                        {
                            errors.code && (
                                <Form.Text className="text-danger">
                                    Please enter a valid gift code.
                                </Form.Text>
                            )
                        }
                    </Form.Group>
                    <Button variant="warning" type="submit" block>
                        {loading ? <i className="fa fa-spinner fa-pulse fa-fw"></i> : 'CLAIM'}
                    </Button>
                </Form>
            </Col>
        </Row>
    </Container>
    );
};

export default BonusCode;