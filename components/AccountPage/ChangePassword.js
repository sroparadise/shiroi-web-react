import {
    Form, 
    Button, 
    Container,
    Col,
    Row,
} from 'react-bootstrap';
import { toast } from 'react-toastify';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { instance as axios } from '../../lib/fetcher';

const ChangePassword = ({ token }) => {
    const {
        register,
        handleSubmit,
        errors,
    } = useForm();

    const [passwordsMatch, setPasswordsMatch] = useState(true);
    const [loading, setLoading] = useState(false);

    const onSubmit = async ({
        password,
        newPassword,
        newPasswordConfirm,
    }, e) => {
        if (newPassword !== newPasswordConfirm) {
            setPasswordsMatch(false);
            return;
        }
        try {
            await axios.put('/password', {
                password,
                newPassword,
            }, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            toast.success('Password was changed successfully!');
            e.target.reset();
        } catch (e) {
            const messages = {
                'invalid_password': `Invalid current password!`,
                'invalid_new_password': `Please enter a valid new password!`,
                'default': `Unknown error - contact administration!`,
            };

            toast.error(messages[e.response.data.message || 'default']);
        } finally {
            setLoading(false);
        }
    };

    return (
        <Container>
            <Row className="justify-content-center">
                <Col md={6}>
                    <Form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
                        <Form.Group>
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                name="password" type="password" ref={register({
                                    required: true,
                                    minLength: 6,
                                    maxLength: 32,
                                })}
                                placeholder="Enter your password" />
                            {
                                errors.password && (
                                    <Form.Text className="text-danger">
                                        Please enter a valid password.
                                    </Form.Text>
                                )
                            }
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>New Password</Form.Label>
                            <Form.Control
                                name="newPassword" type="password" ref={register({
                                    required: true,
                                    minLength: 6,
                                    maxLength: 32,
                                })}
                                placeholder="Enter your new password" />
                            {
                                errors.newPassword && (
                                    <Form.Text className="text-danger">
                                        Please enter a valid new password.
                                    </Form.Text>
                                )
                            }
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Confirm New Password</Form.Label>
                            <Form.Control
                                name="newPasswordConfirm" type="password" ref={register({
                                    required: true,
                                    minLength: 6,
                                    maxLength: 32,
                                })}
                                placeholder="Enter your new password confirmation" />
                            {
                                errors.newPasswordConfirm && (
                                    <Form.Text className="text-danger">
                                        Please enter a valid password confirmation.
                                    </Form.Text>
                                )
                            }
                            {
                                !passwordsMatch && (
                                    <Form.Text className="text-danger">
                                        Confirmation doesn't match the new password.
                                    </Form.Text>
                                )
                            }
                        </Form.Group>
                        <Button variant="warning" type="submit" block>
                            {loading ? <i className="fa fa-spinner fa-pulse fa-fw"></i> : 'CONFIRM'}
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
};

export default ChangePassword;