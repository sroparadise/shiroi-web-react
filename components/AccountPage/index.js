import { useState, useEffect } from 'react';
import { Container, Col, Card, Tabs, Tab, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import ChangePassword from './ChangePassword';
import VoteForSilk from './VoteForSilk';
import LoginPage from '../LoginPage';
import BonusCode from './BonusCode';
import Router, { useRouter } from 'next/router';

const AccountPage = ({
    isAuthenticated,
    token,
}) => {
    const [key, setKey] = useState('vote');

    const router = useRouter();

    useEffect(() => {
        if (router) {
            const splitpath = router.asPath.split('');
            if (new Set(splitpath).has('#')) {
                const route = router.asPath.split('#')[1];               
                if (index[route]) setKey(route);
            }
        }
    });

    const onClickTab = tab => {
        Router.push(`/account#${tab}`);
    };

    const index = {
        'vote': {
            title: "VOTE REWARDS",
            component: <VoteForSilk token={token} />
        },
        'code': {
            title: "GIFT CODE",
            component: <BonusCode token={token} />
        },
        'password': {
            title: "CHANGE PASSWORD",
            component: <ChangePassword token={token} />
        },
    };

    return isAuthenticated ? (
        <div className="page-wrapper space-top-100">
            <Container>
                <Row className="justify-content-center pb-4">
                    <Col xs={12}>
                        <Card bg="t-black" text="white">
                            <Card.Header>
                                <Tabs
                                    className="bg-t-brown"
                                    variant="pills"
                                   
                                    activeKey={key}
                                    onSelect={(k) => onClickTab(k)}>
                                    {
                                        Object.keys(index).map((key, idx) => <Tab key={idx} eventKey={key} title={index[key].title}></Tab>)
                                    }
                                </Tabs>
                            </Card.Header>
                            <Card.Body>
                                { index[key].component }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    ): <LoginPage header="Please sign in to access this page!" />;
};

export default connect(
    (state) => ({
        isAuthenticated: state.session.isAuthenticated,
        token: state.session.token,
    })
)(AccountPage);