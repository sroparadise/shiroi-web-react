import { get } from '../../lib/fetcher';
import useSWR from 'swr';
import { Container, Col, Row, Button } from 'react-bootstrap';
import moment from 'moment';
import { isOpera } from 'react-device-detect';
import Link from 'next/link';

const VoteForSilk = ({ token }) => {
    const {
        data,
        error,
    } = useSWR('/vote', url => get(url, {
        Authorization: `Bearer ${token}`,
    }), { refreshInterval: 60000, loadingTimeout: 300000 });

    const OperaNotice = () => (
        <Container fluid>
            <Row className="vote-wrap">
                <p>Sorry, voting is not allowed on <b>Opera browser</b>!<br />
                    You can download <a href="https://www.google.com/chrome/">Google Chrome</a> or <a href="https://www.microsoft.com/en-us/edge">Microsoft Edge</a> for that.
                </p>
            </Row>
        </Container>
    );

    const VoteComponent = () => (!error && data) ? (
        <Container fluid>
            <Row className="vote-wrap">
                <p>Each vote will give you specified reward instantly after the voting is completed.<br />
                    After the vote a cooldown will be set and time will be shown when your next vote is available.
                </p>
            </Row>
            {
                data.map(({
                    site,
                    url,
                    enabled,
                    next_vote,
                    reward
                }, idx) => (
                    <Row key={idx} className="justify-items-center align-items-center vote-wrap">
                        <Col><b>{site.toUpperCase()}</b></Col>
                        <Col>
                            <b>REWARD <span className="text-danger">{reward} SILK</span></b>
                        </Col>
                        <Col>{enabled ? (<Button enabled={enabled.toString()} block variant="dark" target="blank" href={url}>VOTE</Button>) : `${moment(parseInt(next_vote)).format('DD/MM/YYYY HH:mm:ss')}`}</Col>
                    </Row>
                ))
            }
        </Container>
    ) : error ? (<><b>It appears that the voting service doesn't like your IP address.</b> <Link href="/donate">
        <a>Why not donate?</a>
    </Link></>) : (<><i className="fa fa-spinner fa-pulse fa-fw"></i> Verifying eligibility..</>);

    return isOpera ? <OperaNotice /> : <VoteComponent />;
};

export default VoteForSilk;