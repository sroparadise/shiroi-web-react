import { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import Router from 'next/router';
import { instance as axios } from '../../lib/fetcher';
import Link from 'next/link';
import FadeIn from 'react-fade-in';

import {
    Container,
    Col,
    Row,
    Form,
    Button,
    Card,
} from 'react-bootstrap';

const RegisterPage = ({
    isAuthenticated,
}) => {
    useEffect(() => {
        if (isAuthenticated) Router.push('/');
    }, [isAuthenticated]);

    const [passwordsMatch, setPasswordsMatch] = useState(true);
    const [signupComplete, setSignupComplete] = useState(false);

    const [regEmail, setRegEmail] = useState('');
    const [loading, setLoading] = useState(false);

    const {
        register,
        handleSubmit,
        errors,
    } = useForm();

    const onSubmit = async ({
        username,
        email,
        password,
        confirmPassword,
        agreement,
    }) => {
        if (password !== confirmPassword) {
            setPasswordsMatch(false);
            return;
        }

        try {
            setLoading(true);
            await axios.post('/users', {
                username,
                password,
                email,
                agreement,
            });

            setRegEmail(email);
            setSignupComplete(true);

        } catch (e) {
            setLoading(false);
            const messages = {
                'username_unavailable': `Username "${username}" not available!`,
                'email_unavailable': `Email "${email}" not available!`,
                'invalid_agreement': `You must agree to the Terms of Service, NDA and Privacy Policy before use.`,
                'username_invalid': `Username is not valid!`,
                'failed_to_create': `Failed to create account - please contact administration!`,
                'default': `Unknown error - please contact administration!`,
                'bad_ipaddr': `VPN/Proxy detection returned bad status, please again later!`,
            };

            toast.error(messages[e.response.data.message || 'default']);
        }
    };

    return (
        <div className="page-wrapper space-top-100">
            <Container>
                <Col>
                    <FadeIn>
                        <Row className="justify-content-center">
                            {signupComplete && (
                                <Col md={6}>
                                    <Card>
                                        <Card.Body className="text-center">
                                            <Card.Title>Welcome to Shiroi Online!</Card.Title>
                                            <Card.Text>
                                                Your account has been created and you can now login!
                                                {/* You need to <span>verify your account</span> by clicking the verification link sent to: <b>{regEmail}</b> */}
                                            </Card.Text>
                                            {/* <Card.Subtitle><b>Can't find the email?</b> - Check your spam folder.</Card.Subtitle> */}
                                            <Link href="/login"><a className="text-warning">Sign In</a></Link><br/>
                                            <Link href="/download"><a className="text-info">Download Game</a></Link>
                                        </Card.Body>

                                    </Card>
                                </Col>
                            )}
                            {!signupComplete && (
                                <Col md={6}>
                                    <Card>
                                        <Card.Body>
                                            <Card.Title>Registration</Card.Title>
                                            <Form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
                                                <Form.Group>
                                                    <Form.Label>Username</Form.Label>
                                                    <Form.Control
                                                        placeholder="Enter your username"
                                                        type="text"
                                                        name="username"
                                                        ref={register({
                                                            required: true,
                                                            minLength: 3,
                                                            maxLength: 16,
                                                            pattern: /^[A-Za-z0-9_]+$/i
                                                        })}
                                                    />
                                                    {
                                                        errors.username && (
                                                            <Form.Text className="text-danger">
                                                                Username length must be between 3-16 characters of (A-Za-z0-9_).
                                                            </Form.Text>
                                                        )
                                                    }
                                                </Form.Group>
                                                <Form.Group>
                                                    <Form.Label>E-Mail address</Form.Label>
                                                    <Form.Control
                                                        type="email"
                                                        placeholder="Enter your E-Mail"
                                                        name="email"
                                                        ref={register({
                                                            required: true,
                                                            minLength: 6,
                                                            maxLength: 42,
                                                            pattern: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
                                                        })}
                                                    />
                                                    {
                                                        errors.email && (
                                                            <Form.Text className="text-danger">
                                                                Please enter a valid email address.
                                                            </Form.Text>
                                                        )
                                                    }
                                                </Form.Group>
                                                <Form.Group>
                                                    <Form.Label>Password</Form.Label>
                                                    <Form.Control
                                                        name="password"
                                                        type="password"
                                                        ref={register({
                                                            required: true,
                                                            minLength: 6,
                                                            maxLength: 32,
                                                        })}
                                                        placeholder="Enter your password" />
                                                    {
                                                        errors.password && (
                                                            <Form.Text className="text-danger">
                                                                Password length must be between 6-32 characters.
                                                            </Form.Text>
                                                        )
                                                    }
                                                </Form.Group>
                                                <Form.Group>
                                                    <Form.Label>Password Confirmation</Form.Label>
                                                    <Form.Control
                                                        name="confirmPassword"
                                                        type="password"
                                                        ref={register({
                                                            required: true,
                                                            minLength: 6,
                                                            maxLength: 32,
                                                        })}
                                                        placeholder="Enter password confirmation" />
                                                    {
                                                        errors.password && (
                                                            <Form.Text className="text-danger">
                                                                Password length must be between 6-32 characters.
                                                            </Form.Text>
                                                        )
                                                    }
                                                    {
                                                        !passwordsMatch && (
                                                            <Form.Text className="text-danger">
                                                                Confirmation doesn't match the password.
                                                            </Form.Text>
                                                        )
                                                    }
                                                </Form.Group>
                                                <Form.Group>
                                                    <Form.Check name="agreement" type="checkbox" ref={register} label={
                                                        <>
                                                            I have read all and agree to the "<Link href="/terms-of-service"><a target="blank">Terms of Service</a></Link>"<br /> and "<Link href="/privacy-policy" target="blank"><a>Privacy Policy</a></Link>".
                                                        </>
                                                    } />
                                                </Form.Group>
                                                <Button variant="warning" type="submit" block>
                                                    {loading ? <i className="fa fa-spinner fa-pulse fa-fw"></i> : 'CONFIRM'}
                                                </Button>
                                                <Form.Text className="text-info text-center">
                                                    Already registerd? - <Link href="/login"><a>Login here</a></Link>.
                                            </Form.Text>
                                            </Form>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )}
                        </Row>
                    </FadeIn>
                </Col>

            </Container>
        </div>
    );
};
export default connect(
    (state) => ({
        isAuthenticated: state.session.isAuthenticated,
    })
)(RegisterPage);