import { Button } from 'react-bootstrap';
import { toSvg } from 'html-to-image';

const Snapshot = () => {

    const handle = () => {
        const element = document.querySelector('body');
        toSvg(element)
            .then(dataUrl => {
                var link = document.createElement('a');
                link.download = 'shiroi.svg';
                link.href = dataUrl;
                link.click();
            });
    }

    return (
        <div className="snapshot">
            <Button variant="t-black" onClick={() => handle()}>
                <i className="fa fa-camera"></i>
            </Button>
        </div>
    )
};

export default Snapshot;