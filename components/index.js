import dynamic from 'next/dynamic';

// Shared components:
const Navigation = dynamic(() => import('./Navigation'));
const ScrollTop = dynamic(() => import('./ScrollTop'));
const Logo = dynamic(() => import('./Logo'));
const Header = dynamic(() => import('./Header'));
const Footer = dynamic(() => import('./Footer'));
const Consent = dynamic(() => import('./Consent'));
const Snapshot = dynamic(() => import('./Snapshot'));

// Page views:
const HomePage = dynamic(() => import('./HomePage'));
const DownloadPage = dynamic(() => import('./DownloadPage'));
const LoginPage = dynamic(() => import('./LoginPage'));
const RegisterPage = dynamic(() => import('./RegisterPage'));
const ResendEmailPage = dynamic(() => import('./ResendEmailPage'));
const AccountPage = dynamic(() => import('./AccountPage'));
const WalletPage = dynamic(() => import('./WalletPage'));
const RankingPage = dynamic(() => import('./RankingPage'));
const InfoPage = dynamic(() => import('./InfoPage'));
const RecoverPasswordPage = dynamic(() => import('./RecoverPasswordPage'));
const RecoverPasswordChangePage = dynamic(() => import('./RecoverPasswordPage/ChangePassword'));

export {
    // Shared components:
    Navigation,
    ScrollTop,
    Logo,
    Header,
    Footer,
    Consent,
    Snapshot,

    // Page views:
    HomePage,
    DownloadPage,
    LoginPage,
    RegisterPage,
    ResendEmailPage,
    AccountPage,
    WalletPage,
    RankingPage,
    InfoPage,
    RecoverPasswordPage,
    RecoverPasswordChangePage,
};