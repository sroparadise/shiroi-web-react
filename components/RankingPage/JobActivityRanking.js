import Table from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import useSWR from 'swr';
import { get } from '../../lib/fetcher';

const JobActivityRanking = () => {
    const {
        data,
        error,
    } = useSWR('/ranking?type=job', get, {
        refreshInterval: 150000
    });

    const rowClasses = (row, rowIndex) => {
        switch (rowIndex) {
            case 0:
                return 'bg-gold';
            case 1:
                return 'bg-silver';
            case 2:
                return 'bg-bronze';
            default:
                return;
        }
    };

    const columns = [
        {
            dataField: 'index',
            text: '#',
            headerStyle: () => ({ width: '55px' }),
        },
        {
            dataField: 'NickName16',
            text: 'Alias',
            headerStyle: () => ({ width: '170px' }),
        },
        {
            dataField: 'Class',
            text: 'Class',
        },
        {
            dataField: 'JobLevel',
            text: 'Level',
            headerStyle: () => ({ width: '75px' }),
        },
        {
            dataField: 'JobExp',
            text: 'Experience',
        },
        {
            dataField: 'Rank',
            text: 'Rank',
        },
        {
            dataField: 'ReputationPoint',
            text: 'Reputation',
        },
        {
            dataField: 'KillCount',
            text: 'Player Kills',
        }
    ];

    return (data && !error) ? (<>
        <Table bordered={true} bootstrap4={true} keyField='Name' data={data.map((i, index) => ({ ...i, index: `${index + 1}` }))} columns={columns} rowClasses={rowClasses} />
        {
            !data.length && (
                <p className="text-warning">No records found - you shall be the first one :)</p>
            )
        }
    </>) : <i className="fa fa-spinner fa-pulse fa-fw"></i>;
};

export default JobActivityRanking;