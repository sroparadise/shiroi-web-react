import { connect } from 'react-redux';
import { useState, useEffect } from 'react';
import { Container, Col, Card, Tabs, Tab, Row } from 'react-bootstrap';
import FadeIn from 'react-fade-in';
import JobActivityRanking from './JobActivityRanking';
import ActivityRanking from './ActivityRanking';
import PVPRanking from './PVPRanking';
import Snapshot from '../Snapshot';
import Router, { useRouter } from 'next/router';
import { Header } from '..';
import FortressInfo from '../HomePage/FortressInfo';

const RankingPage = ({
    isAuthenticated,
}) => {
    const [key, setKey] = useState('game-activity');

    const router = useRouter();

    useEffect(() => {
        if (router) {
            const splitpath = router.asPath.split('');
            if (new Set(splitpath).has('#')) {
                const route = router.asPath.split('#')[1];
                if (index[route]) setKey(route);
            }
        }
    });

    const onClickTab = tab => {
        Router.push(`/ranking#${tab}`);
    };

    const index = {
        'game-activity': {
            title: 'Game Activity',
            component: <ActivityRanking />
        },
        // 'pvp': {
        //     title: 'Most dangerous players',
        //     component: <PVPRanking />,
        // },
        'job-activity': {
            title: 'Job Activity',
            component: <JobActivityRanking />,
        },
        // 'job-thief': {
        //     title: 'Strongest Thieves',
        //     component: <JobRanking section="thief" />,
        // },
    };

    return (
        <div className="page-wrapper space-top-100">
            <Header title={index[key].title || 'Ranking'} />
            <Container>
                <Row>
                    <FortressInfo />
                </Row>
                <Row className="justify-content-center">
                    <Col>
                        <FadeIn>
                            <Card bg="t-black" text="white">
                                <Card.Header>
                                    <Tabs
                                        className="bg-t-brown"
                                        variant="pills"
                                        activeKey={key}
                                        onSelect={(k) => onClickTab(k)}>
                                        {
                                            Object.keys(index).map(key => (
                                                <Tab key={key} eventKey={key} title={`${index[key].title}`} />
                                            ))
                                        }

                                    </Tabs>
                                </Card.Header>
                                <Card.Body className="ranking-wrapper pb-3">
                                    {index[key].component}
                                </Card.Body>
                            </Card>
                        </FadeIn>
                    </Col>
                </Row>
            </Container>
            <Snapshot />
        </div>
    );
};

export default connect(
    (state) => ({
        isAuthenticated: state.session.isAuthenticated,
    }),
)(RankingPage);