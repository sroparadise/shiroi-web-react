import Table from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import useSWR from 'swr';
import { get } from '../../lib/fetcher';

const ActivityRanking = () => {
    const {
        data,
        error,
    } = useSWR('/ranking?type=players', get, {
        refreshInterval: 150000
    });

    const rowClasses = (row, rowIndex) => {
        switch (rowIndex) {
            case 0:
                return 'bg-gold';
            case 1:
                return 'bg-silver';
            case 2:
                return 'bg-bronze';
            default:
                return;
        }
    };

    const columns = [
        {
            dataField: 'index',
            text: '#',
            headerStyle: () => ({ width: '55px' }),
        },
        {
            dataField: 'Name',
            text: 'Name',
            headerStyle: () => ({ width: '170px' }),
        },
        {
            dataField: 'QuestPoints',
            text: 'Quests',
            headerStyle: () => ({ width: '75px' }),
        },
        {
            dataField: 'Level',
            text: 'Level',
        },
        {
            dataField: 'HP',
            text: 'Health',
        },
        {
            dataField: 'MP',
            text: 'Mana',
        },
        {
            dataField: 'SP',
            text: 'Skill Points',
        },
        {
            dataField: 'Gold',
            text: 'Gold',
        }
    ];

    return (data && !error) ? (<>
        <Table bordered={true} bootstrap4={true} keyField='Name' data={data.map((i, index) => ({ ...i, index: `${index + 1}` }))} columns={columns} rowClasses={rowClasses} />
        {
            !data.length && (
                <p className="text-warning">No records found - you shall be the first one :)</p>
            )
        }
    </>) : <i className="fa fa-spinner fa-pulse fa-fw"></i>;
};

export default ActivityRanking;