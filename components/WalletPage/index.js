import { Card, Container, Row, Col, Form, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import FadeIn from 'react-fade-in';
import LoginPage from '../LoginPage';
import { instance as axios } from '../../lib/fetcher';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import { get } from '../../lib/fetcher';
import useSWR from 'swr';
import { loadStripe } from '@stripe/stripe-js';

const WalletPage = ({
    isAuthenticated,
    token,
}) => {
    const router = useRouter();
    const [loading, setLoading] = useState(false);

    const {
        data,
        error,
    } = useSWR('/packages', get);

    const {
        watch,
        register,
        handleSubmit,
        errors,
    } = useForm();

    const [total, setTotal] = useState(0);

    const packageId = watch('package');

    useEffect(async () => {
        if (!window.stripe) window.stripe = await loadStripe('pk_live_Cp4VUUFWPiZNRMrpUXGK1eE100IvdBh5wZ');
        if (router.query.completed) {
            switch (router.query.completed) {
                case '1':
                    toast.success('Thank you for donation - your silk was added to your account!');
                    break;
                default:
                    toast.error('Donation was cancelled - changed your mind?');
                    break;
            }
        }
    });

    const onSubmit = async form_data => {
        try {
            setLoading(true);
            const { data } = await axios.post('/payments', form_data, {
                headers: {
                    Authorization: `Bearer ${token}`
                },
            });

            switch (data.provider) {
                case 'paypal':
                    window.location = data.result;
                    break;
                case 'stripe':
                    window.stripe.redirectToCheckout({
                        sessionId: `${data.result}`
                    });
                    break;
            }

        } catch (e) {
            const messages = {
                'invalid_agreement': `Must agree to the terms first!`,
                'invalid_payment_provider': `Payment provider not found!`,
                'invalid_payment_request': `Payment payload is not valid!`,
                'invalid_package': `Requested package doesn't exist!`,
                'default': `Unknown payment error - please contact administration.`,
            };

            toast.error(messages[e.response ? e.response.data.message : 'default']);
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        if (packageId) {
            const pck = data.packages.find(i => i.id === packageId);
            if (!pck) {
                setTotal(0);
                return
            }
            const { amount } = pck;
            setTotal(Math.round(parseInt(amount) + (parseInt(amount) / 100 * parseInt(data.bonus))));
        } else setTotal(0);
    }, [packageId]);

    return isAuthenticated ? (
        <div className="page-wrapper space-top-100">
            <Container>
                <Col>
                    {
                        (!error && data) && (
                            <FadeIn>
                                <Row className="justify-content-center">
                                    <Col md={6}>
                                        <Card>
                                            <Card.Header>
                                                <i className="fa fa-money"></i>{' '}Donate
                                            </Card.Header>
                                            <Card.Body>
                                                {(data.bonus > 0 && data.bonus < 100) && (<p><b>All donations</b> will receive additional <b>{data.bonus}%</b> silk for free!</p>)}
                                                {data.bonus == 100 && (<p><b>DOUBLE TROUBLE!</b> Any donations will receive 2x more than usually!</p>)}
                                                <Form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
                                                    <Form.Group>
                                                        <Form.Label><b>Choose One:</b></Form.Label>
                                                        <Form.Control as="select" name="package" ref={register}>
                                                            <option value={false}>Select package</option>
                                                            {
                                                                data.packages.map(({
                                                                    id,
                                                                    name,
                                                                    cost,
                                                                    amount
                                                                }, i) => (
                                                                    <option key={id} value={id}>{name}: £{cost.toFixed(2)}</option>
                                                                ))
                                                            }
                                                        </Form.Control>
                                                    </Form.Group>


                                                    {
                                                        (total > 0) && (
                                                            <div style={{ textAlign: 'center', padding: '5px', marginBottom: '15px' }}>
                                                                You will receive a total of{` `}<b style={{color:'brown'}}>{total} silk</b>{` `}instantly after payment!
                                                            </div>
                                                        )
                                                    }

                                                    <Form.Group>
                                                        <Form.Label><b>Prefered Payment Option:</b></Form.Label>
                                                        <Form.Control as="select" name="provider" ref={register}>
                                                            <option value="paypal">PayPal Checkout</option>
                                                            <option value="stripe">Bank / Card / Google Pay / Apple Pay</option>
                                                        </Form.Control>
                                                    </Form.Group>


                                                    

                                                    <Form.Group>
                                                        <Form.Check name="agreement" type="checkbox" ref={register} label={
                                                            <>
                                                                I have read and fully agree to the "<Link href="/terms-of-service"><a target="blank">Terms of Service</a></Link>".
                                                            </>
                                                        } />
                                                    </Form.Group>

                                                   

                                                    <Button variant="warning" type="submit" disabled={loading} block>
                                                        {loading ? <i className="fa fa-spinner fa-pulse fa-fw"></i> : 'Continue to Payment'}
                                                    </Button>
                                                </Form>

                                            </Card.Body>
                                        </Card>
                                    </Col>
                                </Row>
                            </FadeIn>
                        )
                    }
                </Col>
            </Container>
        </div>
    ) : <LoginPage header="Please login to access this page" />
};

export default connect(
    (state) => ({
        isAuthenticated: state.session.isAuthenticated,
        token: state.session.token,
    })
)(WalletPage);