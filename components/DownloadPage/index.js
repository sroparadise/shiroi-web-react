import { connect } from 'react-redux';
import { Container, Row, Col, Card, Button, ListGroup } from 'react-bootstrap';
import FadeIn from 'react-fade-in';
import LoginPage from '../LoginPage';
import Header from '../Header';

const DownloadPage = ({
    isAuthenticated,
}) => {
    return (
        <div className="page-wrapper space-top-100">
            <Header title="Downloads" />
            {
                isAuthenticated ? (
                    <Container>
                        <Col>
                            <FadeIn>
                                {/* <Row className="justify-content-center c-downloads">
                                    <Col sm={9}>
                                        <Card bg="t-black" text="white">
                                            <Card.Header className="text-warning">Shiroi Online Launcher/Installer (exe)</Card.Header>
                                            <Card.Body>
                                                <Card.Title>GitHub (80mb)</Card.Title>
                                                <Button href="https://github.com/artuuro/electron-launcher/releases/download/v1.0.1/Shiroi.Connector-1.0.1.Setup.exe" target="blank" variant="warning" block>Download</Button>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                </Row> */}
                                <Row className="justify-content-center c-downloads">
                                    <Col sm={9}>
                                        <Card bg="t-black" text="white">
                                            <Card.Header className="text-warning">Shiroi Online Client (zip)</Card.Header>
                                            <Card.Body>
                                                <Card.Title>Direct Download (2.2GB)</Card.Title>
                                                <Button href="https://api.shiroi.online/dl/sro/shiroi-game.zip" target="blank" variant="warning" block>Download</Button>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                </Row>
                                <Row className="justify-content-center alt-download">
                                    <Col sm={12} lg={9}>
                                        <Card bg="t-black" text="white">
                                            <Card.Header className="text-warning">Other Downloads</Card.Header>
                                            <ListGroup bg="none" variant="flush">

                                                <ListGroup.Item>
                                                    <Row className="justify-content align-items-center">
                                                        <Col sm={1} xs={2}>
                                                            <img src="/r/directx.png" alt="directx" />
                                                        </Col>
                                                        <Col sm={9} xs={10}>
                                                            Microsoft DirectX9
                                                        </Col>
                                                        <Col sm={2} xs={12}>
                                                            <Button target="blank" href="https://directx.softonic.kr/" variant="warning" block>Download</Button>
                                                        </Col>
                                                    </Row>
                                                </ListGroup.Item>
                                                <ListGroup.Item>
                                                    <Row className="justify-content align-items-center">
                                                        <Col sm={1} xs={2}>
                                                            <img src="/r/vcredist.png" alt="msvcx" />
                                                        </Col>
                                                        <Col sm={9} xs={10}>
                                                            Microsoft Visual C++ 2017 Redistributable
                                                        </Col>
                                                        <Col sm={2} xs={12}>
                                                            <Button variant="warning" target="blank" href="https://aka.ms/vs/15/release/vc_redist.x86.exe" block>Download</Button>
                                                        </Col>
                                                    </Row>
                                                </ListGroup.Item>
                                                <ListGroup.Item>
                                                    <Row className="justify-content align-items-center">
                                                        <Col sm={1} xs={2}>
                                                            <img src="/r/dotnet.png" alt="netframework" />
                                                        </Col>
                                                        <Col sm={9} xs={10}>
                                                            Microsoft .NET Framework 4.6.1
                                                        </Col>
                                                        <Col sm={2} xs={12}>
                                                            <Button variant="warning" target="blank" href="https://www.microsoft.com/en-us/download/details.aspx?id=49982" block>Download</Button>
                                                        </Col>
                                                    </Row>
                                                </ListGroup.Item>
                                                <ListGroup.Item>
                                                    <Row className="justify-content align-items-center">
                                                        <Col sm={1} xs={2}>
                                                            <img src="/r/nvidia.png" alt="directx" />
                                                        </Col>
                                                        <Col sm={9} xs={10}>
                                                            Video Drivers for NVidia based GPUs
                                                        </Col>
                                                        <Col sm={2} xs={12}>
                                                            <Button variant="warning" target="blank" href="https://www.geforce.com/drivers" block>Download</Button>
                                                        </Col>
                                                    </Row>
                                                </ListGroup.Item>
                                                <ListGroup.Item>
                                                    <Row className="justify-content align-items-center">
                                                        <Col sm={1} xs={2}>
                                                            <img src="/r/amd.png" alt="directx" />
                                                        </Col>
                                                        <Col sm={9} xs={10}>
                                                            Video Drivers for AMD based GPUs
                                                        </Col>
                                                        <Col sm={2} xs={12}>
                                                            <Button variant="warning" target="blank" href="http://support.amd.com/en-us/download" block>Download</Button>
                                                        </Col>
                                                    </Row>
                                                </ListGroup.Item>
                                                <ListGroup.Item>
                                                    <Row className="justify-content align-items-center">
                                                        <Col sm={1} xs={2}>
                                                            <img src="/r/intel.png" alt="directx" />
                                                        </Col>
                                                        <Col sm={9} xs={10}>
                                                            Video Drivers for Intel based GPUs
                                                        </Col>
                                                        <Col sm={2} xs={12}>
                                                            <Button variant="warning" target="blank" href="https://downloadcenter.intel.com/" block>Download</Button>
                                                        </Col>
                                                    </Row>
                                                </ListGroup.Item>

                                            </ListGroup>
                                        </Card>
                                    </Col>
                                </Row>

                                <Row className="justify-content-center">
                                    <Col sm={12} lg={9}>
                                        <Card bg="t-black" text="white" className="sys-requirements">
                                            <Card.Header className="text-danger" variant="h3">System Requirements</Card.Header>
                                            <Card.Body>
                                                <Card.Title>
                                                    <Row>
                                                        <Col xs={2}></Col>
                                                        <Col xs={5}>Minimum</Col>
                                                        <Col xs={5}>Recommended</Col>
                                                    </Row>
                                                </Card.Title>
                                                <Container>
                                                    <Row>
                                                        <Col xs={2}><b>CPU</b></Col>
                                                        <Col xs={5}>Pentium 3 800MHz or higher</Col>
                                                        <Col xs={5}>Intel i3 or higher</Col>
                                                    </Row>
                                                    <Row>
                                                        <Col xs={2}><b>RAM</b></Col>
                                                        <Col xs={5}>2GB</Col>
                                                        <Col xs={5}>4GB</Col>
                                                    </Row>
                                                    <Row>
                                                        <Col xs={2}><b>Video</b></Col>
                                                        <Col xs={5}>GeForce2 or ATI 9000</Col>
                                                        <Col xs={5}>GeForce FX 5600 / ATI9500 or better</Col>
                                                    </Row>
                                                    <Row>
                                                        <Col xs={2}><b>Sound Card</b></Col>
                                                        <Col xs={5}>DirectX 9.0c Compatibility card</Col>
                                                        <Col xs={5}>DirectX 9.0c Compatibility card</Col>
                                                    </Row>
                                                    <Row>
                                                        <Col xs={2}><b>HDD</b></Col>
                                                        <Col xs={5}>5GB or more</Col>
                                                        <Col xs={5}>7GB or more</Col>
                                                    </Row>
                                                    <Row>
                                                        <Col xs={2}><b>OS</b></Col>
                                                        <Col xs={5}>Windows 7</Col>
                                                        <Col xs={5}>Windows 10</Col>
                                                    </Row>
                                                </Container>

                                            </Card.Body>
                                        </Card>
                                    </Col>
                                </Row>
                            </FadeIn>
                        </Col>
                    </Container>
                ) : (
                    <Container>
                        <FadeIn>
                            <Row className="justify-content-center">
                                <Col sm={12}>
                                    <LoginPage header="Please sign-in to access downloads" />
                                </Col>
                            </Row>
                        </FadeIn>
                    </Container>

                )
            }
        </div >
    );
};

export default connect(({
    session,
}) => ({
    isAuthenticated: session.isAuthenticated,
}))(DownloadPage);