import { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { useForm } from 'react-hook-form';
import Link from 'next/link';
import FadeIn from 'react-fade-in';
import { toast } from 'react-toastify';
import { instance as axios } from '../../lib/fetcher';
import Router, { useRouter } from 'next/router';
import { setSession } from '../../store/slices/sessionSlice';

import {
    Container,
    Col,
    Row,
    Form,
    Button,
    Card,
} from 'react-bootstrap';

const LoginPage = ({
    isAuthenticated,
    setSession,
    header = false,
}) => {
    const router = useRouter();

    useEffect(() => {
        if (isAuthenticated) {
            router.route === '/login' ? Router.push('/') : Router.push(router.route);
        }
    }, [isAuthenticated, router]);

    const [loading, setLoading] = useState(false);

    const {
        register,
        handleSubmit,
        errors,
    } = useForm();

    const onSubmit = async data => {
        try {
            setLoading(true);
            const result = await axios.post('/token', data);
            setSession({
                token: result.data.token,
                username: data.username,
                isAuthenticated: true,
            });
        } catch (e) {
            setLoading(false);

            const messages = {
                'invalid_credentials': `Incorrect username and/or password!`,
                'invalid_verification': (
                    <>
                        Please verify your email first!<br /><Link href="/resend-email"><a>Click here</a></Link> to resend verification.
                    </>
                ),
                'default': `Login failed - please contact administration!`,
            };
            toast.error(messages[e.response.data.message || 'default']);
        }
    };

    return (
        <div className="page-wrapper space-top-100">
            <Container>
                <Col>
                    <FadeIn>
                        <Row className="justify-content-center">
                            <Col md={6}>
                                <Card>
                                    <Card.Body>
                                        <Card.Title>{header ? header : 'Authentication'}</Card.Title>
                                        <Form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
                                            <Form.Group>
                                                <Form.Label>Username</Form.Label>
                                                <Form.Control
                                                    type="text"
                                                    placeholder="Enter your username"
                                                    name="username" ref={register({
                                                        required: true,
                                                        minLength: 3,
                                                        maxLength: 16,
                                                        pattern: /^[A-Za-z0-9_]+$/i
                                                    })}
                                                />
                                                {
                                                    errors.username && (
                                                        <Form.Text className="text-danger">
                                                            Please enter a valid username.
                                                        </Form.Text>
                                                    )
                                                }
                                            </Form.Group>
                                            <Form.Group>
                                                <Form.Label>Password</Form.Label>
                                                <Form.Control
                                                    name="password" type="password" ref={register({
                                                        required: true,
                                                        minLength: 6,
                                                        maxLength: 32,
                                                    })}
                                                    placeholder="Enter your password" />
                                                {
                                                    errors.password && (
                                                        <Form.Text className="text-danger">
                                                            Please enter a valid password.
                                                        </Form.Text>
                                                    )
                                                }
                                            </Form.Group>
                                            <Form.Text className="text-info text-center">
                                                Forgot password? - <Link href="/forgot-password"><a>Recover here</a></Link>.
                                            </Form.Text><br />
                                            <Form.Group>
                                                <Form.Check name="remember" type="checkbox" ref={register} label="Stay logged in for a longer time" />
                                            </Form.Group>
                                            <Button variant="warning" type="submit" block>
                                                {loading ? <i className="fa fa-spinner fa-pulse fa-fw"></i> : 'ENTER'}
                                            </Button>
                                            <Form.Text className="text-info text-center">
                                                Need an account? - <Link href="/register"><a>Register here</a></Link>.
                                            </Form.Text>
                                        </Form>
                                    </Card.Body>
                                </Card>
                            </Col>

                        </Row>
                    </FadeIn>
                </Col>
            </Container>
        </div>
    );
};

export default connect(
    (state) => ({
        isAuthenticated: state.session.isAuthenticated,
    }), {
    setSession,
}
)(LoginPage);