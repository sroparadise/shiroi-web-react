import Link from 'next/link';
import {
    Container,
    Row,
} from 'react-bootstrap';

const Footer = () => (
    <footer className="page-wrapper justify-content-end">
        <Container>
            <Row className="justify-content-center align-content-center">
                <p className="copy">2021 &copy; Shiroi Online</p>
            </Row>
            <Row className="justify-content-center">
                <Link href="/privacy-policy">
                    <a>
                        Privacy Policy
                    </a>
                </Link>
                <Link href="/terms-of-service">
                    <a>
                        Terms of Service
                    </a>
                </Link>
                <a target="blank" href="https://discord.gg/AmdD2mxQTq">
                    Community
                </a>
            </Row>
            <Row className="justify-content-center">
                <a title="Shiroi Silkroad ElitePvPers" href="https://www.elitepvpers.com/forum/sro-pserver-advertising/4701930-shiroi-online-cap130-15d-sro-r-new-old-school-latest-isro-features.html" target="blank">
                    <img className="forums" alt="elitepvpers" src="/r/epvp.png" />
                </a>
                <a title="Best silkroad server" href="https://silkroad-servers.com/index.php?a=in&u=shiroionline" target="blank" >
                    <img className="forums" src="https://silkroad-servers.com/button.php?u=shiroionline&buttontype=static" />
                </a>
                <a href="http://www.xtremetop100.com/in.php?site=1132371318" target="blank" title="Silkroad Online Private Server">
                    <img className="forums" src="https://www.xtremeTop100.com/votenew.jpg" border="0" alt="Silkroad Online Private Server" />
                </a>
            </Row>
        </Container>
    </footer>
);
export default Footer;