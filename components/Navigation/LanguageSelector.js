import { useState } from 'react';
import { useRouter } from 'next/router';
import { NavDropdown } from 'react-bootstrap';
import Link from 'next/link';

const LanguageTitle = ({ choice }) => {
    const config = {
        'en': {
            flag: 'gb',
            name: 'English'
        },
        'ru': {
            flag: 'ru',
            name: 'Русский',
        },
        // 'tr': {
        //     flag: 'tr',
        //     name: 'Türkçe'
        // },
        // 'de': {
        //     flag: 'de',
        //     name: 'Deutsch'
        // },
        // 'es': {
        //     flag: 'es',
        //     name: 'Español'
        // },
        'ar': {
            flag: 'ps',
            name: 'العربية'
        },
    };

    return (
        <>
            <i className={`flag-icon flag-icon-${config[choice].flag}`}></i> {config[choice].name}
        </>
    );
};

const LanguageSelector = () => {
    const router = useRouter();

    const [show, setShow] = useState(false);

    return (
        <NavDropdown
            drop="left"
            title={<LanguageTitle choice={router.locale} />}
            onToggle={() => setShow(!show)}
            show={show}>
            {[
                'en',
                'ru',
                // 'de',
                // 'es',
                'ar',
            ].filter(locale => locale !== router.locale).map((name, index) => (
                <Link key={index} href={router.pathname} locale={name} preload="false">
                    <a className="dropdown-item" onClick={() => setShow(false)}>
                        <LanguageTitle choice={name} />
                    </a>
                </Link>
            ))}
        </NavDropdown>
    );
};

export default LanguageSelector;