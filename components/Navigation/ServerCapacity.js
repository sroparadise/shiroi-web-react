import useSWR from 'swr';
import { get } from '../../lib/fetcher';
import { round } from 'lodash';

const ServerCapacity = () => {
    const {
        data,
        error,
    } = useSWR('/status', get, { 
       refreshInterval: 10000 
    });

    const capacityClassName = ({
        count,
        capacity,
    }) => {
        const percentage = round(count * 100 / capacity, 2);

        const colorConfig = [{
            min: 0,
            max: 24.99,
            className: 'easy',
        }, {
            min: 25,
            max: 49.99,
            className: 'populated',
        }, {
            min: 50,
            max: 74.99,
            className: 'busy',
        }, {
            min: 75,
            max: 100,
            className: 'crowded',
        }];
        const { className } = colorConfig.find(i => i.min <= percentage && i.max >= percentage);
        return className;
    };

    return (error || !data) ? (
        <i className="fa fa-spinner fa-pulse fa-fw"></i>
    ) : (
        <>
            <span className={`${capacityClassName(data)}`}>{data.count} / {data.capacity}</span>
        </>
    );
};
export default ServerCapacity;