import {
    Navbar,
    Nav,
    Row,
    Col,
    Container,
    Badge,
} from 'react-bootstrap';
import ServerCapacity from './ServerCapacity';
import ServerTime from './ServerTime';
import Link from 'next/link';
import FadeIn from 'react-fade-in';
import UserBox from './UserBox';
import { connect } from 'react-redux';
import LanguageSelector from './LanguageSelector';
import useTranslation from '../../lib/useTranslation';

const Navigation = _ => {
    const t = useTranslation();

    return t.nav ? (
        <div className="nav-wrapper">
            <div className="infobar">
                <Container>
                    <FadeIn>
                        <Col>
                            <Row className="justify-content-between">
                                <Nav className="align-items-center">
                                    <Nav.Item>
                                        <Badge variant="secondary">
                                            <ServerTime />
                                        </Badge>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Badge variant="secondary">
                                            <ServerCapacity />
                                        </Badge>
                                    </Nav.Item>
                                </Nav>
                                <Nav className="justify-content-end">
                                    <Nav.Item>
                                        <LanguageSelector />
                                    </Nav.Item>
                                    <Nav.Link target="blank" href="https://www.facebook.com/gameshiroi">
                                        <i className="fa fa-facebook"></i>
                                    </Nav.Link>
                                    <Nav.Link target="blank" href="https://discord.gg/AmdD2mxQTq">
                                        <i className="fa fa-discord"></i>
                                    </Nav.Link>
                                </Nav>
                            </Row>
                        </Col>
                    </FadeIn>
                </Container>
            </div>
            <div className="main">
                <Container>
                    <FadeIn>
                        <Navbar collapseOnSelect expand="lg" variant="default">
                            <Navbar.Brand>
                                <Link href="/">
                                    <a>
                                        <img src="/icon.png" alt="shiroi icon" />
                                    </a>
                                </Link>
                            </Navbar.Brand>
                            <Navbar.Toggle aria-controls="site-menu">
                                <i className="fa fa-bars" />
                            </Navbar.Toggle>
                            <Navbar.Collapse id="site-menu">
                                <Nav className="mr-auto">
                                    <Link href="/info#rates">
                                        <a className="nav-link">{t.nav.info || "Info"}</a>
                                    </Link>
                                    <Link href="/ranking">
                                        <a className="nav-link">{t.nav.hiscores || "Ranking"}</a>
                                    </Link>
                                    <Nav.Link target="blank" href="https://discord.gg/AmdD2mxQTq">
                                        {t.nav.forums || "Forums"}
                                    </Nav.Link>
                                    <Link href="/download">
                                        <a className="nav-link">{t.nav.downloads || "Downloads"}</a>
                                    </Link>
                                </Nav>
                                <UserBox />
                            </Navbar.Collapse>
                        </Navbar>
                    </FadeIn>
                </Container>
            </div>

        </div>
    ) : (<></>);
};

export default connect(({
    session,
}) => ({
    isAuthenticated: session.isAuthenticated,
}))(Navigation);