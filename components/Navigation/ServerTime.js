import useSWR from 'swr';
import { get } from '../../lib/fetcher';
import moment from 'moment';
import { useEffect, useState } from 'react';

const ServerTime = () => {
    const {
        data,
        error,
    } = useSWR('/time', get, {
        refreshInterval: 0
    });

    const [timestamp, setTimestamp] = useState();

    useEffect(() => {
        if (data) {
            setTimestamp(data.timestamp);            
        }
    }, [data]);

    useEffect(() => {
        let timeout;
        if (timestamp) {
           timeout = setTimeout(() => setTimestamp(moment(timestamp).utc().add(1, 'seconds')), 1000);
        }
        return () => timeout ? clearTimeout(timeout) : null;
    }, [timestamp]);

    return timestamp ? (
        <span>{moment(timestamp).utc().format('DD/MM/YYYY HH:mm:ss')}</span>
    ) : (
        <i className="fa fa-spinner fa-pulse fa-fw"></i>
    );
};
export default ServerTime;