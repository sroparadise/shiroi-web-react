import Link from 'next/link';
import { Nav } from 'react-bootstrap';
import useTranslation from '../../../lib/useTranslation';

const LoggedOut = () => {
    const t = useTranslation();

    return t.nav ?  (
        <Nav className="auth-links">
            <Link href="/register">
                <a className="nav-link btn btn-xs btn-default">{t.nav.register}</a>
            </Link>
            <Link href="/login">
                <a className="nav-link btn btn-xs btn-default">
                    {t.nav.login}{' '}<i className="fa fa-sign-in"></i>
                </a>
            </Link>
        </Nav>
    ) : (<></>);
};

export default LoggedOut;