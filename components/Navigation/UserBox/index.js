import { connect } from 'react-redux';
import Logged from './Logged';
import LoggedOut from './LoggedOut';

const UserBox = ({
    isAuthenticated,
}) => isAuthenticated ? <Logged /> : <LoggedOut />;

export default connect(
    (state) => ({
        isAuthenticated: state.session.isAuthenticated,
    })
)(UserBox);