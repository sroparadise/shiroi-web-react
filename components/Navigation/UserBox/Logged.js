import { connect } from 'react-redux';
import { useState, useEffect } from 'react';
import { unsetSession } from '../../../store/slices/sessionSlice';
import { Nav } from 'react-bootstrap';
import Link from 'next/link';
import { get } from '../../../lib/fetcher';
import useSWR from 'swr';
import useTranslation from '../../../lib/useTranslation';

const Logged = ({
    username,
    token,
    unsetSession,
}) => {
    const signOut = () => unsetSession();
    //const [points, setPoints] = useState(0)
    const [silk, setSilk] = useState(0);

    const t = useTranslation();

    const {
        data,
        error,
    } = useSWR('/credits', url => get(url, {
        Authorization: `Bearer ${token}`,
    }), { refreshInterval: 5000 });

    useEffect(() => {
        if (data) {
            //setPoints(data.silk_point);
            setSilk(data.silk_own);
        }
    });

    return t.nav ? (
        <Nav className="auth-links align-items-center">
            <Nav.Item>
                <span className="nav-link welcome">{t.nav.welcome}</span>
            </Nav.Item>
            <Link href="/account">
                <a className="nav-link btn btn-xs btn-default">{username}</a>
            </Link>
            <Link href="/donate">
                <a className="nav-link btn btn-xs btn-default">
                    <img src="/sro/silk_premium.gif" alt="silk" /> {silk}
                </a>
            </Link>
            <a onClick={signOut} className="nav-link login btn btn-xs btn-default">
                {t.nav.logout} <i className="fa fa-sign-out"></i>
            </a>
        </Nav>
    ) : (<></>);
};


export default connect(
    (state) => ({
        username: state.session.username,
        token: state.session.token,
    }), {
    unsetSession,
}
)(Logged);
