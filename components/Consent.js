import CookieConsent from 'react-cookie-consent';
import Link from 'next/link';

const Consent = () => {
    return (
        <CookieConsent
            location='bottom'
            buttonText='Agree'
            style={{
                background: '#000',
                zIndex: 99999,
                alignItems: 'center',
                width: 'calc(100% - 40px)',
                maxWidth: '840px',
                padding: '10px 20px',
                left: '20px',
                marginBottom: '20px',
                border: '1px solid rgba(255, 255, 255, .3)'
            }}
            buttonStyle={{
                background: '#dc3545',
                fontSize: '1.2rem',
                padding: '10px 20px'
            }}>
            <p style={{
                margin: 0
            }}>
                <b style={{ fontSize: '1.2rem' }}>We are using localStorage and cookies to enhance the user experience.</b><br />
                If you continue to use this site you agree with our <Link href="/terms-of-service"><a className="text-warning">Terms of Service</a></Link> and <Link href="/privacy-policy"><a className="text-warning">Privacy Policy</a></Link>.
            </p>

        </CookieConsent>
    );
};

export default Consent;