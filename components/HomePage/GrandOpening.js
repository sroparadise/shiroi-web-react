import { useEffect, useState } from 'react';
import Countdown from 'react-countdown';
import moment from 'moment';
import { Card } from 'react-bootstrap';

const GrandOpening = () => {
    const time = moment("2021-03-13T15:00:00Z").utcOffset(0);
    const isServer = typeof window === 'undefined';
    const o = i => i < 10 ? `0${i}` : i;

    const renderer = ({ days, hours, minutes, seconds, completed }) => {
        if (completed) {
            return <span>Ready to Play!</span>;
        } else {
            return <span className="countdown">{o(days)}d {o(hours)}:{o(minutes)}:{o(seconds)}</span>;
        }
    };

    const [timer, setTimer] = useState(false);

    useEffect(() => {
        if (!isServer) {
            setTimer(true);
        }
    });

    return (
        <Card bg="t-black" text="white" className="game-events">
            <Card.Header className="text-danger text-center" as="h5">
                <i className="fa fa-clock-o"></i>{' '}GRAND OPENING COUNTDOWN
            </Card.Header>
            <Card.Body className="text-center">
                <h1 className="text-warning">
                    {
                        timer && <Countdown
                            ssr={false}
                            date={new Date(time)}
                            renderer={renderer}
                        />
                    }
                </h1>
            </Card.Body>
        </Card>
    )
};

export default GrandOpening;