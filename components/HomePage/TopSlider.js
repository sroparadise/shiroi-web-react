import {
    Carousel,
} from 'react-bootstrap';


const TopSlider = () => (
    <Carousel fade={true} data-aos="fade-in">
        <Carousel.Item>
            <img
                className="d-block w-100"
                src="/slides/1.jpg"
                alt="Attend!"
            />
            <Carousel.Caption  className="slider-content">
                <h3>Fun and addictive!</h3>
                <p>Enjoy the game with no premium limits, play with ultimately low latency - have great fun and activity.</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
            <img
                className="d-block w-100"
                src="/slides/2.jpg"
                alt="Fight!"
            />

            <Carousel.Caption className="slider-content">
                <h3>Take yourself to the next level!</h3>
                <p>Gear up and eliminate powerful foes - get stronger, improve your equipment and gain silk from uniques.</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
            <img
                className="d-block w-100"
                src="/slides/3.jpg"
                alt="Conquer!"
            />

            <Carousel.Caption  className="slider-content">
                <h3>Fight for control!</h3>
                <p>Be a mighty Thief or Hunter and dominate the opposing force, craft Jobbing equipment and push your power to the limits.</p>
            </Carousel.Caption>
        </Carousel.Item>
    </Carousel>
);

export default TopSlider;