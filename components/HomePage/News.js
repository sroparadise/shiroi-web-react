import useSWR from 'swr';
import { get } from '../../lib/fetcher';
import moment from 'moment';
import HTMLParser from 'react-html-parser';
import { Card } from 'react-bootstrap';
import { v4 as uuid } from 'uuid';
import Linkify from 'react-linkify';


const News = () => {
    const {
        data,
        error,
    } = useSWR('/news', get, {
        refreshInterval: 150000
    });

    return (
        <Card bg="t-black" text="white" className="game-events">
            <Card.Header className="text-danger" as="h5"><i className="fa fa-newspaper-o"></i>{' '}Game News & Updates</Card.Header>
            <Card.Body>
                {
                    (!error && data) ? data.map(({
                        id = uuid(),
                        subject,
                        content,
                        date,
                    }) => (
                        <div key={id}>
                            <Card.Title className="pt-2">[{moment(date).format('DD/MM/YYYY')}]: {subject}</Card.Title>
                            <Card.Text className="text-pre-wrap">
                                <Linkify>{HTMLParser(content)}</Linkify>
                            </Card.Text>
                            <Card.Subtitle className="text-info"></Card.Subtitle>
                        </div>
                    )) : <i className="fa fa-spinner fa-pulse fa-fw"></i>
                }
            </Card.Body>
        </Card>
    );
};

export default News;