import { Container, Row, Col } from 'react-bootstrap';
import Link from 'next/link';
import News from './News';
import TopSlider from './TopSlider';
import GameEvents from './GameEvents';
//import GrandOpening from './GrandOpening';
//import Logo from '../Logo';
import FadeIn from 'react-fade-in';
import { connect } from 'react-redux';
import { useRouter } from 'next/router';
import useTranslation from '../../lib/useTranslation';

const HomePage = ({
    isAuthenticated,
    username,
}) => {
    const router = useRouter();
    const t = useTranslation();
    return t.home ? (
        <div className="page-wrapper space-top-100">
            <Container>
                <FadeIn>
                    <Row>
                        <Col sm={8}>
                            {/* <Row data-aos="fade-in">
                                <Container fluid>
                                    <TopSlider />
                                </Container>
                            </Row> */}
                            {/* <Row>
                                <Container fluid>
                                    <GrandOpening />
                                </Container>
                            </Row> */}
                            <Row>
                                <Container fluid>
                                    <News />
                                </Container>
                            </Row>
                        </Col>
                        <Col sm={4}>
                            <Row>
                                <Container fluid>
                                    <a
                                        href="https://api.shiroi.online/dl/sro/shiroi-game.zip"
                                        target="_blank"
                                        className="btn btn-danger btn-block play-now"><i className="fa fa-file-zip-o" /> {t.home.download}</a>
                                </Container>
                            </Row>

                            {
                                !isAuthenticated && (
                                    <>
                                        <Row>
                                            <Container fluid>
                                                <Link href="/register">
                                                    <a className="btn btn-darnger btn-block play-now purple"><i className="fa fa-user-plus" /> {t.home.joinnow}</a>
                                                </Link>
                                            </Container>
                                        </Row>
                                    </>
                                )
                            }

                            {
                                isAuthenticated && (
                                    <>
                                        <Row>
                                            <Container fluid>
                                                <Link href="/account#vote">
                                                    <a className="btn btn-darnger btn-block play-now purple"><i className="fa fa-hand-o-right" /> {t.home.freesilk}</a>
                                                </Link>
                                            </Container>
                                        </Row>
                                    </>
                                )
                            }

                            {
                                !router.query.crawler && (
                                    <Row>
                                        <Container fluid>
                                            <GameEvents />
                                        </Container>
                                    </Row>
                                )
                            }

                        </Col>
                    </Row>
                </FadeIn>
            </Container>
        </div>
    ) : (<></>);
};

export default connect(
    (state) => ({
        isAuthenticated: state.session.isAuthenticated,
        username: state.session.username,
    })
)(HomePage);