import { Card } from 'react-bootstrap';
import { get } from '../../lib/fetcher';
import { useEffect, useState } from 'react';
import GameEventsHelper from '../../lib/game_events';
import useSWR from 'swr';

const GameEvents = () => {
    const {
        data,
        error,
    } = useSWR('/schedule', get, {
        refreshInterval: 60000,
    });

    const [events, setEvents] = useState([]);

    useEffect(() => {
        if (data) setEvents(new GameEventsHelper(data, 20).result());
    }, [data]);

    const index = {
        'SCHEDULE_DEF_MEET_ROC': 'Attack the Roc',
        'SCHEDULE_DEF_CHINS_TOMB': 'Qin-Shi Tomb (Medusa)',
        'SCHEDULE_DEF_SPECIAL_GOODS_SELL': 'Special Trade Goods',
        'SCHEDULE_DEF_MENTOR_HONOR_UPDATE': 'Honor Ranking Update',
        // 'SCHEDULE_DEF_SIEGE_REQUEST_PERIOD': 'Fortress Request Period',
        'SCHEDULE_DEF_SIEGE_PERIOD': 'Fortress War',
        //'SCHEDULE_DEF_SIEGE_FORTRESS_DB_UPDATE': 'Fortress Update',
        //'SCHEDULE_DEF_SIEGE_TAX_PERIOD': 'Fortress War Tax',
        'SCHEDULE_DEF_EGYPT_TEMPLE_OF_SELKIS_AND_NEITH': 'Temple [Selket and Neith]',
        'SCHEDULE_DEF_EGYPT_TEMPLE_OF_ANUBIS_AND_ISIS': 'Temple [Anubis and Isis]',
        'SCHEDULE_DEF_EGYPT_TEMPLE_HAROERIS_AND_SETH': 'Temple [Haroeris and Seth]',
        'SCHEDULE_DEF_FLAG_WORLD_PARTICIPATION': 'Capture the Flag',
        // 'SCHEDULE_DEF_FLAG_WORLD': 'Capture the Flag',
        // 'SCHEDULE_DEF_BATTLE_ARENA_MATCH_POINT': 'Capture the Flag [point game]',
        // 'SCHEDULE_DEF_BATTLE_ARENA_MATCH_FLAG': 'Capture the Flag [flag game]',
        'SCHEDULE_DEF_BATTLE_ARENA_RANDOM_PARTICIPATION': 'Battle Arena [all-random]',
        // 'SCHEDULE_DEF_BATTLE_ARENA_RANDOM': 'Battle Arena Random',
        'SCHEDULE_DEF_BATTLE_ARENA_PARTY_PARTICIPATION': 'Battle Arena [party]',
        //'SCHEDULE_DEF_BATTLE_ARENA_PARTY': 'Battle Arena (15min left)',
        'SCHEDULE_DEF_BATTLE_ARENA_GUILD_PARTICIPATION': 'Battle Arena [guild]',
        //'SCHEDULE_DEF_BATTLE_ARENA_GUILD': 'Battle Arena Guild',
        'SCHEDULE_DEF_BATTLE_ARENA_JOB_PARTICIPATION': 'Battle Arena [job]',
        //'SCHEDULE_DEF_BATTLE_ARENA_JOB': 'Battle Arena Job',
        'SCHEDULE_DEF_EVENT_EUBUSINESS': 'Bonus Exp Ratio',
        'SCHEDULE_DEF_EVENT_MINI_GAME': 'Mini Game',
        'SCHEDULE_DEF_EVENT_IMPROVE_TRI_JOB_EXP': 'Improved Jobbing Exp',
        'SCHEDULE_DEF_EVENT_GOLD_WEEK_1': 'Gold Week 1',
        'SCHEDULE_DEF_EVENT_GOLD_WEEK_2': 'Gold Week 2',
        'SCHEDULE_DEF_EVENT_GOLD_WEEK_3': 'Gold Week 3',
        'SCHEDULE_DEF_EVENT_GOLD_WEEK_4': 'Gold Week 4',
        'SCHEDULE_DEF_EVENT_UNIQUE_MONSTER_SPAWN': 'Titan Uniques',
    };

    return (
        <Card bg="t-black" text="white" className="game-events">
            <Card.Header as="h5" className="text-danger"><i className="fa fa-clock-o"></i>{' '}Upcoming Events</Card.Header>
            <Card.Body>
                {
                    events.length ? events.map(({ id, name, start_time, tts }) => (
                        index[name] && <div key={id}>
                            <Card.Title>
                                {index[name]}
                            </Card.Title>
                            <Card.Subtitle>Event time: <span className="text-warning">{start_time}</span></Card.Subtitle>
                            <Card.Text>
                                Time till start: <span className="text-danger">{tts}</span>
                            </Card.Text>
                        </div>
                    )) : <i className="fa fa-spinner fa-pulse fa-fw"></i>
                }
            </Card.Body>
        </Card>
    );
};

export default GameEvents;