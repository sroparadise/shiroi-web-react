import ReactPlayer from 'react-player/lazy';
import {  
    ResponsiveEmbed,
} from 'react-bootstrap';

const VideoFrame = () => (
    <div className="video-wrapper">
        <ResponsiveEmbed aspectRatio="16by9">
            <ReactPlayer
                url="/v/intro.mp4"
                loop={true}
                muted={true}
                volume={0}
                playing={true}
                width="100%"
                height="100%"
            />
        </ResponsiveEmbed>
    </div>
);

export default VideoFrame;