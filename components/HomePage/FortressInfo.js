import { Card } from 'react-bootstrap';
import { get } from '../../lib/fetcher';
import { v4 as uuid } from 'uuid';
import { Col } from 'react-bootstrap';
import useSWR from 'swr';

const FortressInfo = () => {
    const {
        data,
        error,
    } = useSWR('/fortress', get);

    return !error && data ? data.map(({
        id = uuid(),
        name,
        icon,
        guild_name,
        tax_ratio,
        tax,
    }) => (
        <Col lg={3} sm={6} key={id}>
            <Card text="white" bg="t-black">
                <Card.Body>
                    <Card.Title className="text-center">
                        <img src={icon} alt="name" /> {name}
                    </Card.Title>
                    <Card.Subtitle className="text-center">
                        <span className="text-danger">{(guild_name == '-') ? `Not Occupied` : guild_name}</span>
                    </Card.Subtitle>
                    <Card.Text className="text-center">
                        Tax: <span className="text-warning">{String(tax).replace(/(.)(?=(\d{3})+$)/g,"$1'")}</span> ({tax_ratio}%)
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    )) : <i className="fa fa-spinner fa-pulse fa-fw"></i>;
};

export default FortressInfo;