import { Row } from 'react-bootstrap';

const Logo = () => {
    return (
        <Row className="justify-content-center">
            <img src="/icon.png" alt="Shiroi Online" className="top-logo" />
        </Row>
    );
};

export default Logo;