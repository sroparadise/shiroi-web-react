import { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { useForm } from 'react-hook-form';
import Link from 'next/link';
import FadeIn from 'react-fade-in';
import { toast } from 'react-toastify';
import { instance as axios } from '../../lib/fetcher';
import Router from 'next/router';

import {
    Container,
    Col,
    Row,
    Form,
    Button,
    Card,
} from 'react-bootstrap';

const RecoverPasswordPage = ({
    isAuthenticated,
}) => {
    useEffect(() => {
        if (isAuthenticated) Router.push('/');
    }, [isAuthenticated]);
   
    const [loading, setLoading] = useState(false);
    const [emailSent, setEmailSent] = useState(false);
    const [regEmail, setRegEmail] = useState('');

    const {
        register,
        handleSubmit,
        errors,
    } = useForm();

    const onSubmit = async data => {
        setLoading(true);
        try {
            await axios.put('/users', data);
            setEmailSent(true);
            setRegEmail(data.email);
        } catch (e) {
            setLoading(false);

            const messages = {
                'email_not_found': `User was not found!`,
                'password_invalid': `Invalid Password.`,
                'email_invalid': `Invalid E-Mail.`,
                'default': `Unknown error - contact administration please`,
            };

            toast.error(messages[e.response.data.message || 'default']);
        }
    };

    return (
        <div className="page-wrapper space-top-100">
            <Container>
                <Col>
                    <FadeIn>
                        <Row className="justify-content-center">
                            {emailSent && (
                                <Col md={6}>
                                    <Card>
                                        <Card.Body className="text-center">
                                            <Card.Title>A link has been sent!</Card.Title>
                                            <Card.Text>
                                                You should receive the email at this address: <b>{regEmail}</b>
                                            </Card.Text>
                                            <Card.Subtitle><b>Can't find the email?</b> - Check your spam folder.</Card.Subtitle>
                                            <Link href="/"><a className="text-warning">Return Home</a></Link>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )}
                            {!emailSent && (
                                <Col md={6}>
                                    <Card>
                                        <Card.Body>
                                            <Card.Title>Recover Password</Card.Title>
                                            <Form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
                                                <Form.Group>
                                                    <Form.Label>E-Mail address</Form.Label>
                                                    <Form.Control
                                                        type="email"
                                                        placeholder="Enter your E-Mail"
                                                        name="email"
                                                        ref={register({
                                                            required: true,
                                                            minLength: 6,
                                                            maxLength: 42,
                                                            pattern: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
                                                        })}
                                                    />
                                                    {
                                                        errors.email && (
                                                            <Form.Text className="text-danger">
                                                                Please enter a valid email address.
                                                            </Form.Text>
                                                        )
                                                    }
                                                </Form.Group>
                                                <Button variant="warning" type="submit" block>
                                                    {loading ? <i className="fa fa-spinner fa-pulse fa-fw"></i> : 'RECOVER'}
                                                </Button>
                                            </Form>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )}

                        </Row>
                    </FadeIn>
                </Col>

            </Container>
        </div>
    );
};

export default connect(
    (state) => ({
        isAuthenticated: state.session.isAuthenticated,
    })
)(RecoverPasswordPage);