import {
    Button,
    Container,
    Col,
    Row,
    Form,
    Card,
} from 'react-bootstrap';
import { toast } from 'react-toastify';
import { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { instance as axios } from '../../lib/fetcher';
import { unsetSession } from '../../store/slices/sessionSlice';
import { connect } from 'react-redux';
import FadeIn from 'react-fade-in';
import Router from 'next/router';

const ChangePassword = ({
    token,
    unsetSession,
    isAuthenticated,
}) => {
   

    useEffect(() => {
        if (!isAuthenticated) Router.push('/login');
    }, [isAuthenticated]);

    const {
        register,
        handleSubmit,
        errors,
    } = useForm();
    const signOut = () => unsetSession();
    const [passwordsMatch, setPasswordsMatch] = useState(true);
    const [loading, setLoading] = useState(false);

    const onSubmit = async ({
        newPassword,
        newPasswordConfirm,
    }, e) => {
        if (newPassword !== newPasswordConfirm) {
            setPasswordsMatch(false);
            return;
        }
        try {
            await axios.put('/password', {
                newPassword,
            }, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            toast.success('Password was updated successfully - please sign in again!');
            e.target.reset();
            signOut();
        } catch (e) {
            const messages = {
                'invalid_new_password': `Please enter a valid new password!`,
                'default': `Unknown error - contact administration!`,
            };

            toast.error(messages[e.response.data.message || 'default']);
        } finally {
            setLoading(false);
        }
    };

    return (

        <div className="page-wrapper space-top-100">
            <Container>
                <Col>
                    <FadeIn>
                        <Row className="justify-content-center">
                            <Col md={6}>
                                <Card>
                                    <Card.Body>
                                        <Card.Title>Change Password</Card.Title>
                                        <Form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
                                            <Form.Group>
                                                <Form.Label>New Password</Form.Label>
                                                <Form.Control
                                                    name="newPassword" type="password" ref={register({
                                                        required: true,
                                                        minLength: 6,
                                                        maxLength: 32,
                                                    })}
                                                    placeholder="Enter your new password" />
                                                {
                                                    errors.newPassword && (
                                                        <Form.Text className="text-danger">
                                                            Please enter a valid new password.
                                                        </Form.Text>
                                                    )
                                                }
                                            </Form.Group>
                                            <Form.Group>
                                                <Form.Label>Confirm New Password</Form.Label>
                                                <Form.Control
                                                    name="newPasswordConfirm" type="password" ref={register({
                                                        required: true,
                                                        minLength: 6,
                                                        maxLength: 32,
                                                    })}
                                                    placeholder="Enter your new password confirmation" />
                                                {
                                                    errors.newPasswordConfirm && (
                                                        <Form.Text className="text-danger">
                                                            Please enter a valid password confirmation.
                                                        </Form.Text>
                                                    )
                                                }
                                                {
                                                    !passwordsMatch && (
                                                        <Form.Text className="text-danger">
                                                            Confirmation doesn't match the new password.
                                                        </Form.Text>
                                                    )
                                                }
                                            </Form.Group>
                                            <Button variant="warning" type="submit" block>
                                                {loading ? <i className="fa fa-spinner fa-pulse fa-fw"></i> : 'CONFIRM'}
                                            </Button>
                                        </Form>
                                    </Card.Body>
                                </Card>
                            </Col>

                        </Row>
                    </FadeIn>
                </Col>
            </Container>
        </div>

    );
};

export default connect(
    (state) => ({
        username: state.session.username,
        token: state.session.token,
        isAuthenticated: state.session.isAuthenticated,
    }), {
    unsetSession,
}
)(ChangePassword);