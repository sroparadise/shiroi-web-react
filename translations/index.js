import dynamic from 'next/dynamic';

const English = dynamic(() => import('./en.json'));
const Russian = dynamic(() => import('./ru.json'));
const Arabic = dynamic(() => import('./ar.json'));

export default {
    en: English,
    ru: Russian, 
    ar: Arabic,
};