import { Header, Logo } from '../components';
import { Container, Row, Col } from 'react-bootstrap';
import Link from 'next/link';

const Error404 = () => {
  return (
    <>
      <Header title="Not Found" />
      <div className="page-wrapper justify-content-start space-top-100">
        <Container>
          <Row>
            <Col>
              <Row className="justify-content-center">
                <div className="loading text-center bg-t-black">
                  <h3 className="text-danger">Nothing here.. maybe check if its on our discord tho..</h3>
                  <a href="https://discord.gg/AmdD2mxQTq" target="blank" className="text-warning">VISIT DISCORD</a>
                </div>
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  )
};

export default Error404;