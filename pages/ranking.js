import { Header, RankingPage } from '../components';

const register = () => (
    <>
        <Header title="Player Ranking" />
        <RankingPage />
    </>
);

export default register;