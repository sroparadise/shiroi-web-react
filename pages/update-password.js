import { Header, RecoverPasswordChangePage } from '../components';


const resendEmail = () => (
    <>
        <Header title="Update Password" />
        <RecoverPasswordChangePage />
    </>
);

export default resendEmail;