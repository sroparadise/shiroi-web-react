import 'bootstrap/dist/css/bootstrap.min.css';
import 'flag-icon-css/css/flag-icon.min.css';
import 'react-toastify/dist/ReactToastify.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import '../styles/index.css';
import NextApp from 'next/app';
import withReduxStore from '../lib/withReduxStore';
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore } from 'redux-persist';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import { Navigation, ScrollTop, Snapshot, Consent } from '../components';
import AOS from 'aos';


class App extends NextApp {
  componentDidMount() {
    AOS.init();
  }

  render() {
    const { Component, reduxStore } = this.props;
    const persistor = persistStore(reduxStore);

    return (
      <Provider store={reduxStore}>
        <PersistGate loading={null} persistor={persistor}>
          {
            () => (
              <div id="app">
                <Navigation />
                <Component />
                <ToastContainer position="bottom-center" autoClose={3000} />

                <div className="scroll-top-wrap"><ScrollTop /></div>
                <div className="consent-wrap"><Consent /></div>
                {/* <Snapshot /> */}
              </div>
            )
          }
        </PersistGate>
      </Provider>
    );
  }
}

export default withReduxStore(App);