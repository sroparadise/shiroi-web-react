import { Header, WalletPage } from '../components';

const account = () => {
    return (
        <>
            <Header title="Donate" />
            <WalletPage />
        </>
    );
};

export default account;