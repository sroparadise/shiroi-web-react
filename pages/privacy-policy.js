import Link from 'next/link';
import FadeIn from 'react-fade-in';
import { Container, Row, Col, Card } from 'react-bootstrap';
import { Header, Logo } from '../components';

const PrivacyPolicy = () => {
    return (
        <>
            <Header title="Privacy Policy" />
            <div className="page-wrapper justify-content-start">
                <Container>
                    <Col>
                        <Logo />
                        <Row className="justify-content-center">
                            <FadeIn>
                                <Card>
                                    <Card.Body>
                                        <Card.Title>Privacy Policy</Card.Title>
                                        <Card.Subtitle>Effective from: 2020.01.01</Card.Subtitle>
                                        <Card.Text>
                                            <p>
                                                This Privacy and Cookies Policy (hereinafter referred to as - Policy) describes how shiroi.online will
                                                collect, store, access, use, process or disclose Your information and data.
                                                It applies and covers all the Website and Platforms under domain shiroi.online as well as its sub-domains
                                                (hereinafter referred to as-Website and Platform) and all of shiroi.online, Website, and Platform applications and services.
                                                </p>
                                            <p>
                                                If You do not wish to give consent for the actions, conditions, provisions and terms set in this
                                                Policy You should stop using the Website and Platform immediately.
                                                </p>
                                            <p>
                                                The Data Protection Act 1998 (c 29) is a United Kingdom Act of Parliament designed to protect
                                                personal data stored on computers or in an organised paper filing system. It follows the EU Data Protection Directive
                                                1995 protection, processing and movement of data.
                                                </p>
                                            <ul>
                                                <li>
                                                    <p>
                                                        shiroi.online will only collect, keep, use and share Your information either for genuine business
                                                        purposes that shiroi.online has explained clearly to You and that You haven’t objected to, or where shiroi.online has legally
                                                        required to do so – and as soon as those purposes have been fully achieved, shiroi.online will delete Your information;
                                                        </p>
                                                </li>
                                                <li>
                                                    <p>shiroi.online will always be ready to explain and inform You about the reasons and purposes of collecting and using of Your information;</p>
                                                </li>
                                                <li>
                                                    <p>shiroi.online will protect Your information it as if it was its own information, using appropriate security safeguards;</p>
                                                </li>
                                                <li>
                                                    <p>shiroi.online will never use Your information for other purposes than is defined by law and this Policy.</p>
                                                </li>
                                            </ul>
                                        </Card.Text>
                                        <Card.Subtitle>INFORMATION BEING COLLECTED</Card.Subtitle>
                                        <Card.Text>
                                            <p>
                                                In order to carry out our services of the Website and Platform to You and improve Your user experience shiroi.online mostly automatically and in some cases manually collects the following information:
                                                </p>
                                            <ul>
                                                <li>
                                                    <p>
                                                        Personal Information: such as IP address, name, email, company address, billing address and information, company description, phone number, address and location.
                                                        Browser information: such as location, browser and operating system versions.
                                                        Payment information: such as credit card or debit card details, financial account information and billing information.
                                                        Identity Verification: Date of birth, age, taxpayer identification, company registration number and similar information in order to verify Your identity and comply with government regulations or institutional requests. In some cases (set forth in Terms and Conditions), shiroi.online may also request photocopies of identity documents or a billing statement or copy of a utility bill if shiroi.online requires additional proof of confirmation of Your identity.
                                                        </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        Non-Identifying information: Postal codes, usage time and visit session durations, visited pages, demographic information or any aggregated information made available through Your browser when visiting the Website and/or using the Platform, and its services.
                                                        </p>
                                                </li>
                                            </ul>
                                            <p>
                                                Please note that the use of the Website and Platform is indented for users aged 18 years or older.
                                                In the case where unknowingly collected Information of persons under the legal age of 18 years, shiroi.online will take reasonable measures
                                                to delete such information. If You are a parent or a guardian of a person under the legal age of 18 years, please contact us by opening a support ticket after you login to your account for a request of removing such data.
                                                        </p>
                                            <p>shiroi.online collects Your information in various ways. shiroi.online may also get information from other sources and may combine it with information shiroi.online collected about You.</p>
                                            <ol>
                                                <li>
                                                    <b>Information You submit to shiroi.online.</b>
                                                    <p>By visiting and Using the Website and its Services, for example, by registering Your User Account, filling in the User profile data, using shiroi.online payment system, communication with us, You give shiroi.online Your personal, payment and identity information, for example, name, address, telephone number, e-mail address, bank account, payment method, date of birth, identification number, personal preferences etc.</p>
                                                    <p>All this information requires a direct action from You in order for shiroi.online to receive it.</p>
                                                </li>
                                                <li>
                                                    <b>Information that shiroi.online collects automatically.</b>
                                                    <p>shiroi.online automatically receives certain types of Your information whenever You use Platform and its services. This certain information gets created and recorded automatically by the IT systems necessary to operate the Website, Platform and their applications and its services.</p>
                                                    <p>That way shiroi.online collect the information about Your device, Your IP address, Your computer, the date and time of Your access the Platform, the type of browser You use, Your online activities.</p>
                                                    <p>shiroi.online use “cookies” and "LocalStorage" (a small piece of data sent from a Website and stored on the Your computer by Your browser while You are browsing. Cookies is a reliable mechanism for websites to remember stateful information or to record the user's browsing activity and to remember arbitrary pieces of information that the You previously entered into form fields) to make it easier for You to use the Platform. These cookies may be “session” cookies (that last until You exit the Platform) or “persistent” cookies (that last until You or Your browser deletes them).</p>
                                                    <p>Some of the cookies shiroi.online uses are directly associated with Your User Account.</p>
                                                    <p>Other Users, advertisers and advertising networks that serve ads on the Website may also use their own mechanisms, like cookies. These third-party cookies are governed by the privacy policies of the entities placing the ads and are not subject to this Policy.</p>
                                                    <p>By using the Platform and Website, You agree to shiroi.online using the cookies. To delete any cookies that are already on Your computer, the “help” section in Your internet browser should provide instructions on how to locate the file or directory that stores cookies. Please note that by deleting or disabling cookies, You may not be able to take advantage of certain functions of the Website and Platform.</p>
                                                </li>
                                                <li>
                                                    <b>Information that You give Your permission to obtain from other of Your accounts</b>
                                                    <p>
                                                        Depending on Your settings or the privacy policies for other online services, You may give
                                                        shiroi.online a permission to obtain information from this online services (for example if You sign to the Platform via Facebook
                                                        or other social media channel, or via Your mobile) You may at Your discretion, give shiroi.online permission to access
                                                        Your information in that social media channel or in other services. The information shiroi.online obtain from those
                                                        services does depend on Your settings for that service or their privacy policies, and You should always regularly check what those are.

                                                        </p>
                                                </li>
                                            </ol>
                                        </Card.Text>
                                        <Card.Subtitle>USE OF INFORMATION</Card.Subtitle>
                                        <Card.Text>
                                            <p>
                                                shiroi.online uses Your information for a variety of purposes, including providing You with Platform services. The purposes of use of Your information are as follows:
                                                </p>
                                            <ul>
                                                <li>
                                                    Process Your orders, proposals, offers, requests, payments and other services, You obtain by using the Platform,
                                                    Protect shiroi.online’s and other Users and third-party rights and property.
                                                    </li>
                                                <li>
                                                    Respond to legal process and requests of governmental, law enforcement body or court or other authorized institution
                                                    or organization. Develop or inform You of new products and services.
                                                    </li>
                                                <li>
                                                    Anonymize or aggregate Your information for various purposes like market analysis and reporting.
                                                    Customize or personalize Your experience with Platform services.
                                                    </li>
                                                <li>
                                                    Customize or personalize advertising and communications to bring You information about products and
                                                    services that may interest You. Monitor, evaluate or improve Platform services, systems, or networks.
                                                    </li>
                                            </ul>
                                        </Card.Text>
                                        <Card.Subtitle>SHARED INFORMATION</Card.Subtitle>
                                        <Card.Text>
                                            <p>
                                                shiroi.online may share Your information to:
                                                </p>
                                            <ul>
                                                <li>
                                                    shiroi.online payment system provider – shiroi.online may share Your personal, identity, payment
                                                    information to shiroi.online payment System provider, to provide You with valid and wholesome system to make and receive payments for
                                                    other User services and products You obtain via Platform;
                                                    </li>
                                                <li>
                                                    Governmental, law enforcement body, court or other authorized institution or organization and banks - shiroi.online may share Your personal, identity, payment and other information to Governmental, law enforcement body,
                                                    court or other authorized institution or organization and banks in case of grounded and legal requests;
                                                    </li>
                                                <li>
                                                    Platform Users – the information You provide in Your User Profile about Your activities, services
                                                    and products, and definitely not Your sensitive data, is available to other Users, for the purpose for You to
                                                    communicate to other Users and to make orders, offers, proposals and conclude agreements about provision of services and products (User to User Agreements).
                                                    </li>
                                                <li>
                                                    Other Third Parties with Your consent- shiroi.online may share information with other third parties only with Your
                                                    consent. Use of the information You agree to share will be subject to those third parties' separate privacy policies.
                                                    </li>
                                                <li>
                                                    shiroi.online may also share information that is anonymized or in an aggregated form that does not
                                                    directly identify You to other service providers and third parties who perform services on behalf of shiroi.online.
                                                    </li>
                                                <li>
                                                    Business Transfers. Personal information about You may be disclosed as part of any merger,
                                                    acquisition, sale of shiroi.online assets or transition of Platform to another provider. In the unlikely event of an
                                                    insolvency, bankruptcy or receivership, information may also be transferred as a business asset.
                                                    </li>
                                            </ul>
                                            <p>
                                                Protection of shiroi.online and Others. shiroi.online may access, monitor, use or disclose Your
                                                personal, identity, payment and other information or communications to do things like Protection of shiroi.online and Others. shiroi.online may access, monitor, use or disclose Your
                                                personal, identity, payment and other information or communications to do things like:

                                                comply with the law or respond to lawful requests or legal process;
                                                protect the rights or property of shiroi.online, shiroi.online and its representatives, including, but
                                                not limited to shiroi.online’ directors, members of the board, actioners, officers, employees, agents, authorized persons and
                                                subcontractors, other Users, third-parties, and others, including to enforce shiroi.online’s agreements, policies and
                                                terms and conditions. respond to emergencies; initiate, render, bill, and collect for services; or
                                                facilitate or verify the appropriate calculation of taxes, fees, or other obligations due to a
                                                local, state, or federal government requirement; or determine eligibility for government benefits.
                                                </p>
                                        </Card.Text>
                                        <Card.Subtitle>PROTECTING YOUR SECURITY</Card.Subtitle>
                                        <Card.Text>
                                            <p>
                                                As soon as shiroi.online receive Your information, shiroi.online use various security features and
                                                procedures, taking into account industry standards, to try to protect the information that You provide and to prevent
                                                unauthorized access to that information. shiroi.online will do all the best to protect Your information and data, still You,
                                                as User, have to keep Your User Account, User Profile data and all the information and data You issue, send, post,
                                                publish share etc on/or via the Platform and Website and/or with other Users and/or with third parties, safe by using
                                                a strong password and by complying with obligations, set forth in Terms and Conditions. As User communication
                                                via Platform is not encrypted, shiroi.online also would recommend not communicating any confidential information
                                                through Platforms communication tools.
                                                </p>
                                        </Card.Text>
                                        <Card.Subtitle>INFORMATION CHOICES AND CHANGES</Card.Subtitle>
                                        <Card.Text>
                                            <p>
                                                You can request a copy of the personal, identity, payment and other data shiroi.online hold on our
                                                systems about You. You may ask shiroi.online to disclose to You the personal, identity, payment and other information
                                                shiroi.online has about You in written form. shiroi.online will require You to provide shiroi.online with the proof of identity and
                                                supporting documents for shiroi.online to clearly identify You. shiroi.online can prepare and provide this information to You within 30 days.
                                                </p>
                                            <p>
                                                Where Your requests are manifestly unfounded or excessive, in particular because of their repetitive character
                                                shiroi.online may charge a reasonable fee taking into account the administrative costs of providing the information or
                                                communication or taking the action requested or refuse to act on the request.
                                                </p>
                                            <p>
                                                shiroi.online will delete/erase Your information within 30 days of Your User Account closure, except as noted below.
                                                </p>
                                            <p>
                                                Also, You may ask shiroi.online to delete/erase Your data and information shiroi.online has about You, or
                                                change/rectify the inaccurate or incomplete information and data. The erasure or deletion of Your data and information
                                                by Your request may cause the closure of Your User Account.

                                                </p>
                                            <p>
                                                shiroi.online may keep some of Your information, to the extent of and if it is reasonably necessary to
                                                comply with shiroi.online legal obligations (including law enforcement requests), meet regulatory requirements, resolve
                                                disputes, maintain security, prevent fraud and abuse.

                                                </p>
                                        </Card.Text>
                                        <Card.Subtitle>CONTACTS</Card.Subtitle>
                                        <Card.Text>
                                            If You have any questions about, or complaints that concern this Policy, please contact our support by opening a ticked when signed in.
                                             </Card.Text>
                                        <Card.Subtitle>CHANGES, UPDATES AND AMENDMENTS</Card.Subtitle>
                                        <Card.Text>
                                            <p>
                                                By using the Website and Platform, You also agree to the Terms and Conditions set forth here.
                                                 </p>
                                            <p>
                                                The last changes are displayed at the beginning of the Policy as “Effective Date” and shiroi.online retains the right to
                                                change or update this Policy, when required and as shiroi.online sees fit. shiroi.online is not required
                                                to inform You of any changes in the Policy personally or separately but may do so electronically using the email address You
                                                provided during sing up in cases You see fit.
                                                     </p>
                                            <p>
                                                shiroi.online may occasionally update or change this Policy. When and if any changes to Policy will be
                                                posted, they will be posted on this page and the date of these changes will be as per “Effective Date” at the top of
                                                this document. Should shiroi.online feels these changes require shiroi.online to inform You separately, shiroi.online will notify You by email using the account email information You provided during Your sign-up process.
                                                     </p>
                                            <p>
                                                If You continue the use of the Platform and the Website You subject Yourself to the current Policy and confirm Your consent to the conditions set forth in this document.
                                                                </p>
                                        </Card.Text>
                                        <Link href="/"><a className="text-warning">Return Home</a></Link>
                                    </Card.Body>
                                </Card>
                            </FadeIn>
                        </Row>
                    </Col>

                </Container>
            </div>
        </>
    );
};

export default PrivacyPolicy;