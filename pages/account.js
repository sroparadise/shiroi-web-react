import { Header, AccountPage } from '../components';

const account = () => {
    return (
        <>
            <Header title="My Account" />
            <AccountPage />
        </>
    );
};

export default account;