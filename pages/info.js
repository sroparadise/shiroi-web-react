import { Header, InfoPage } from '../components';

const register = () => (
    <>
        <Header title="Game Information" />
        <InfoPage />
    </>
);

export default register;