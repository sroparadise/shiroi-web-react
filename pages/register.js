import { Header, RegisterPage } from '../components';

const register = () => (
    <>
        <Header title="Register" />
        <RegisterPage />
    </>
);

export default register;