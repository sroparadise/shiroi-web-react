import Link from 'next/link';
import FadeIn from 'react-fade-in';
import { Container, Row, Col, Card } from 'react-bootstrap';
import { Header, Logo } from '../components';

const TermsOfService = () => {
    return (
        <>
            <Header title="Terms of Service" />
            <div className="page-wrapper justify-content-start">
                <Container>
                    <Col>
                        <Logo />
                        <Row className="justify-content-center">
                            <FadeIn>
                                <Card>
                                    <Card.Body>
                                        <Card.Title>Terms of Service</Card.Title>
                                        <Card.Subtitle>If you continue to use this website, you agree to comply with and be bound by the following terms and conditions of use.</Card.Subtitle>
                                        <Card.Text>
                                            <b>You agree:</b>
                                            <ul>
                                                <li>
                                                    to receive promotion e-mails from Shiroi Online upon registering an account on this website.
                                                    </li>
                                                <li>
                                                    Shiroi team will not get involved if third party players offend you or your belongings in ingame chat.
                                                    </li>
                                                <li>
                                                    account's safety is player's responsibility and Shiroi team will not get involved with a character hack/scam issue. Players are expected to use an unique password and character lock feature in the game to avoid a potential account hacking or scam.
                                                    </li>
                                                <li>
                                                    to allow us to temporarily block the account(s) and investigate if suspicious activity is detected (such as using illegal software (such is virtual machine, modified dll and such). You also agree that you will have to provide us a photoproof of your devices in order to get your blocked accounts back.
                                                    </li>
                                                <li>
                                                    that you will never attempt to exchange/sell in-game currency for cash.
                                                    </li>
                                                <li>
                                                    donations are voluntary and non-refundable under any circumstance.
                                                    </li>
                                            </ul>
                                            <strong>
                                                Shiroi Online team holds rights to temporarily or even permanently block users without having to make a statement about it if the user somewhat harm Shiroi Online reputation in the game or on the third party platforms.
                                                </strong>
                                        </Card.Text>
                                        <Card.Text>
                                            <b>You also accept the rules:</b>
                                            <p>
                                                By registering an account on Shiroi Online, you accept that you have read and will follow all the rules listed below. If you break any of the rules stated below, a punishment will apply which might include:
                                                </p>
                                            <p>
                                                - Temporary account block
                                                - Permanent account block
                                                - Permanent IP & PC block
                                                - Permanent block for all the accounts associated with your IP and PC
                                                - Permanent block for your character name
                                                    </p>
                                            <p>
                                                IP and HWID (PC) blocks are lifetime - meaning they will also affect Shiroi servers that might be released in the next years.
                                                    </p>
                                            <p>
                                                Upon breaking any of the rules below after first warning a punishment decision is being made.
                                                    </p>
                                            <ul>
                                                <li>
                                                    <b>Racism</b>
                                                    <p>Racism is no good and the admins might punish you temporarily or permanently upon their own decision and depending on the player's attitude. This rule apply to global chat only. If someone is bothering you in DM chat, simply block his DM.</p>
                                                </li>
                                                <li>
                                                    <b>Religions & Believes</b>
                                                    <p>
                                                        Sending globals regarding the believes and religions is not allowed even if you are not offending. Religions and believes are personal stuff and must be kept out of the public chat. This rule apply to global chat only. If someone is bothering you in DM chat, simply block his DM.
                                                        </p>
                                                </li>
                                                <li>
                                                    <b>Selling Silk-Items</b>
                                                    <p>
                                                        Attempting to sell silk-items-account is strictly forbidden. This also include exchanging items in other servers.
                                                        </p>
                                                </li>
                                                <li>
                                                    <b>Bug Abuse</b>
                                                    <p>Abusing any kind of bug on purpose is strictly forbidden.</p>
                                                </li>
                                                <li>
                                                    <b>Advertisements</b>
                                                    <p>Promoting other servers (in any chat) is strictly forbidden.</p>
                                                </li>
                                                <li>
                                                    <b>Virtual Machine </b>
                                                    <p>Virtual Machine use is strictly forbidden and will be treated as dll bypass. We have technical ability to detect Virtual Machine users and every account associated with that Virtual Machine will be affected.</p>
                                                </li>
                                                <li>
                                                    <b>Dll Bypass</b>
                                                    <p>Using third party software to bypass the limits is strictly forbidden. We have technical ability to detect dll bypassers and every account associated with dll bypasser will be affected.</p>
                                                </li>
                                                <li>
                                                    <b>Account Hacking</b>
                                                    <p>Hacking into someone elses account and causing any harm on the account will not be tolerated.</p>
                                                </li>
                                                <li>
                                                    <b>Illegal Software</b>
                                                    <p>
                                                        Using any kind of software other than the softwares listed below is strictly forbidden.
                                                        -Mbot, Sbot, PHBot Alchemy/Fuse Tool or similar.
                                                        </p>
                                                </li>
                                                <li>
                                                    <b>VPN/Proxy Use</b>
                                                    <p>
                                                        VPN/Proxy use is strictly forbidden. If you really in need of a VPN, you must inform Shiroi Staff by contacting them on Discord / Website ticket system.
                                                        </p>
                                                </li>
                                                <li>
                                                    <b>Provocative Communication</b>
                                                    <p>
                                                        Spreading rumors and/or unconfirmed reports related to game or service, sending provocative globals, bullying-flaming the server or Shiroi Team members will not be tolerated.
                                                        </p>
                                                </li>
                                                <li>
                                                    <b>Hosting Events</b>
                                                    <p>
                                                        Hosting Events in the game is not allowed. Players are tend to do that when they decide to quit and giveaway their items. This spreads negativity via globals and we don't like this.
                                                        </p>
                                                </li>
                                                <li>
                                                    <b>Inappropriate Use of Bots</b>
                                                    <p>
                                                        Using bots to camp on unique spawn points is not allowed. This would only kill the joy and we don't want this to happen
                                                        </p>
                                                </li>
                                                <li>
                                                    <b>
                                                        Posting Personal Information
                                                        </b>
                                                    <p>
                                                        Sharing personal information in global chat is strictly forbidden. This might include social media links, phone number, email adress or any data that is belong to somebody. (Not necessarily a player but anyone).
                                                        </p>

                                                </li>
                                                <li>
                                                    <b>
                                                        Inappropriate Global Use
                                                        </b>
                                                    <p>
                                                        Using the global chat is simply speaking out loud to thousands of players. Know what you saying. Sharing links in global chat is limited to screenshots and videos. Sending any other links in global chat might result in punishment.
                                                            </p>
                                                </li>
                                                <li>
                                                    <b>
                                                        Scamming Players
                                                        </b>
                                                    <p>
                                                        Attempting to scam players in gold, silk or in any way such as:
                                                        * Stalling an item for low amount of gold and then changing it to high right before enabling the purchase
                                                        * Scamming players in Dungeon Entry gold
                                                        * Using tricks to fool the players with lower tier item via exchange is not tolerated.
                                                        </p>
                                                </li>
                                                <li>
                                                    <b>
                                                        Using Safezone Buffers
                                                        </b>
                                                    <p>
                                                        Abusing safe zone buffers or buffers without cape is strictly forbidden.
                                                        </p>
                                                </li>
                                            </ul>
                                        </Card.Text>

                                        <Card.Title>Donations & Refunds</Card.Title>
                                        <Card.Subtitle>Eligibility for Refund</Card.Subtitle>
                                        <Card.Text>
                                            <p>You may be eligible for refund if you purchased 'Shiroi Coins' and haven't played the game for more than 2 hours within 14 days of purchase and haven't purchased any items in ingame store.</p>
                                            <p>You will not be eligible for refunds if have been banned or violated the terms of use / server rules. In addition, you may not be eligible for refunds if Shiroi Online determines that you are abusing the refund policy.</p>
                                        </Card.Text>
                                        <Card.Subtitle>How can I request a refund?</Card.Subtitle>
                                        <Card.Text>
                                            <p>You can request a refund by opening a support ticket after you login to your account.</p>
                                        </Card.Text>
                                        <Card.Subtitle>How will I get my refund?</Card.Subtitle>
                                        <Card.Text>
                                            <p>By default, purchases will be refunded to the payment method used to make the purchase, but this may not always be possible depending on the payment method. You and Shiroi Online may also agree that the refund be issued by alternative method. Please note the amount of time for the refund to process is dependent on your payment method.</p>
                                        </Card.Text>

                                        <Link href="/"><a className="text-warning">Return Home</a></Link>
                                    </Card.Body>
                                </Card>
                            </FadeIn>
                        </Row>
                    </Col>
                </Container>
            </div>
        </>
    );
};

export default TermsOfService;