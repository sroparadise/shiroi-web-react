import { Header, ResendEmailPage } from '../components';

const resendEmail = () => (
    <>
        <Header title="Resend Verification" />
        <ResendEmailPage />
    </>
);

export default resendEmail;