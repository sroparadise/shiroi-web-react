import { Header, RecoverPasswordPage } from '../components';

const resendEmail = () => (
    <>
        <Header title="Password Recovery" />
        <RecoverPasswordPage />
    </>
);

export default resendEmail;