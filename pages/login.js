import { Header, LoginPage } from '../components';

const login = () => (
    <>
        <Header title="Authentication" />
        <LoginPage />
    </>
);
export default login;