import { Header, Footer, HomePage } from '../components';

const index = () => {
  return (
    <>
      <Header title="SRO Private Server" />
      <HomePage />
      <Footer />
    </>
  );
};

export default index;