import { connect } from 'react-redux';
import { useEffect } from 'react';
import { toast } from 'react-toastify';
import { setSession } from '../../store/slices/sessionSlice';
import { Container, Row, Col } from 'react-bootstrap';
import Router, { useRouter } from 'next/router';
import { Header } from '../../components';
import { instance as axios } from '../../lib/fetcher';

const verification = ({
    isAuthenticated,
    setSession,
}) => {
    const router = useRouter();
    
    useEffect(() => {
        if (isAuthenticated) Router.push('/');
    }, [isAuthenticated]);

    useEffect(async () => {
        const { token } = router.query;
        if (token && !isAuthenticated) {
            try {
                const result = await axios.get('/token', {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    }
                });

                setSession({
                    token: result.data.token,
                    username: result.data.username,
                    isAuthenticated: true,
                });

                toast.success('Your account is now active!');

            } catch (e) {
                const messages = {
                    'already_verified': `Your account is already verified!`,
                    'reference_not_found': `Couldn't locate referencing account - please contact administration!`,
                    'default': `Validation failed - please contact administration!`,
                };

                toast.error(messages[e.response.data.message || 'default']);
            } finally {
                Router.push('/');
            }
        }
    }, [router.query]);

    return (
        <>
            <Header title="Verification" />
            <div className="page-wrapper justify-content-start space-top-100">
                <Container>
                    <Row>
                        <Col>
                            <Row className="justify-content-center">
                                <div className="loading text-center bg-t-black">
                                    <div className="text-warning text-center">
                                        <i className="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                    <div className="text-warning">
                                       Please wait - verifying this for you...
                                    </div>
                                </div>
                                
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </div>
        </>
    );
};

export default connect(
    (state) => ({
        isAuthenticated: state.session.isAuthenticated,
    }), {
    setSession,
})(verification);