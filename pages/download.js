import { Header, DownloadPage } from '../components';

const index = () => (
  <>
    <Header title="Game Download" />
    <DownloadPage />
  </>
);

export default index;